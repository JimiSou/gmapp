// recalculating inputs values
function recalculateAll(){
  countInEffects();
  var strength=$('#strengthRelative'),
      intelligence=$('#intelligenceRelative'),
      dexterity=$('#dexterityRelative'),
      health=$('#healthRelative'),
      hitPoints=$('#hitPointsRelative'),
      will=$('#willRelative'),
      perception=$('#perceptionRelative'),
      fatiguePoints=$('#fatiguePointsRelative'),
      basicLift=$('#character_basicLift'),
      basicSpeed=$('#character_basicSpeed'),
      basicMove=$('#character_basicMove'),
      hiking=$('#character_hiking'),
      damageThrust=$('#character_damageThrust'),
      damageSwing=$('#character_damageSwing'),
      dodge=$('#character_dodge'),
      startingCP=$('#character_startingCP'),
      gainedCP=$('#character_gainedCP'),
      unusedCP=$('#character_unusedCP'),
      skillsCosts=$('#character_skillsCosts'),
      appearanceLevel=$('#character_appearance'),
      reactionModifier=$('#character_reactionModifier');


  will.html(parseInt(intelligence.html()));
  hitPoints.html(parseInt(strength.html()));
  perception.html(parseInt(intelligence.html()));
  fatiguePoints.html(parseInt(health.html()));
  basicLift.val((strength.html()*strength.html())/5);
  basicSpeed.val((parseInt(dexterity.html())+parseInt(health.html()))/4);
  basicMove.val(parseInt(basicSpeed.val()));
  hiking.val(basicMove.val()*10);
  var damageThrustText="";
  var damageSwingText="";
  $.each(handsDamageTable,function(index,value){
    if(value["strength"]==strength.html())
    {
      damageThrustText=value["thrust"];
      damageSwingText=value["swing"];
      return;
    }
  });
  damageThrust.val(damageThrustText);
  damageSwing.val(damageSwingText);
  dodge.val(parseInt(basicSpeed.val())+3);
  unusedCP.val(parseInt(gainedCP.val())+parseInt(startingCP.val())+parseInt(countAllCPCosts()));
  $('#skillShop_counter_myCp').html(unusedCP.val());
  $.each(appearanceLevelTable,function(index,value){
    if(value["level"]==appearanceLevel.val())
    {
      appearanceLevel.next().attr('title',value["name"]+'['+value['cost']+'] - '+value["description"]);
      return;
    }
  });
  $('#mainAttributes .mainAttributes_field').each(function allMainAttributesInputs(){
    var toto=$(this);
    if(toto.val<=basicAttributeLevelTable[0])
    {
      toto.attr('title',basicAttributeLevelTable[0]["name"]+' - '+basicAttributeLevelTable[0]["description"]);
    }
    else {
      if(toto.val>=basicAttributeLevelTable[basicAttributeLevelTable.length])
      {
        toto.attr('title',basicAttributeLevelTable[basicAttributeLevelTable.length]["name"]+' - '+basicAttributeLevelTable[basicAttributeLevelTable.length]["description"]);
      }
      else {
        $.each(basicAttributeLevelTable,function(index,value){
          if(value["level"]==toto.val())
          {
            toto.attr('title',value["name"]+' - '+value["description"]);
            return;
          }
        });
      }
    }

  });
  //relative values check
  $('#mainAttributes .relativeMod').each(function(){
    var val=parseInt($(this).html());

    if(val==0)
    {
      var id = '#character_'+$(this).attr('id').replace('Relative','');
      $(this).html($(id).val());
    }
  });
  inputDynamicWidth();
  countWeight();
  loadBuyableEffects($('#skillShop_myList'));
}

function countAllCPCosts()
{
  var totalCosts = 0;
  var appearanceLevel = $('#character_appearance').val();
  $.each(appearanceLevelTable,function(index,value){
    if(value["level"]==appearanceLevel)
    {
      totalCosts += parseInt(value["cost"]);
    }
  });
  var buyableCost = $('#character_effects').val();
  if(buyableCost!=='')
  {
    buyableCost=JSON.parse(buyableCost);
    $.each(buyableCost.buyable,function(index,value){
        totalCosts += parseInt(value["cost"]);
    });
  }


    return totalCosts;

}
function countWeight()
{
  var encumbranceTotal=0;
  var totalweight=0;
  $('.inventoryItem').each(function allItems(){
    totalweight+= parseFloat($(this).attr('data-weight'))*parseInt($(this).attr('data-count'));
  });
  $('#itemInventoryWeight').html(totalweight);
  encumbranceTotal+=totalweight;

  var totalweight=0;
  $('.inventoryFirearm').each(function allFirearms(){
    totalweight+= parseFloat($(this).attr('data-weight'))*parseInt($(this).attr('data-count'));
  });
  $('#firearmInventoryWeight').html(totalweight);
  encumbranceTotal+=totalweight;

  var totalweight=0;
  $('.inventoryWearable').each(function allWearables(){
    totalweight+= parseFloat($(this).attr('data-weight'))*parseInt($(this).attr('data-count'));
  });
  $('#wearableInventoryWeight').html(totalweight);
  encumbranceTotal+=totalweight;

  var totalweight=0;
  $('.inventoryMeelee').each(function allMeelees(){
    totalweight+= parseFloat($(this).attr('data-weight'))*parseInt($(this).attr('data-count'));
  });
  $('#meeleeInventoryWeight').html(totalweight);
  encumbranceTotal+=totalweight;

  var basicLift=$('#character_basicLift').val();
  $.each(encumbranceLevelTable, function(i, value) {

    if(basicLift*value.basicLift>=encumbranceTotal)
    {
      $('#character_encumbranceLevel').val(value.level);
      $('#encumbranceLevelLabel').html(value.label);
      return false;
    }
  });
}


function loadSkillsLevels()
{
  var skillsLevels = $('#character_skillsLevels');
  var skillsLevelsArray = skillsLevels.val();
  if (skillsLevelsArray == '')
  {
    $('#skills .skillItem').each(function loadDefaults(){
      var toto=$(this);
      var checkbox = toto.find('.skillItemCheckbox');
      var defaultModifier = checkbox.attr('data-skill-default');
      var attribute = checkbox.attr('data-skill-attribute');
      toto.find('.skillItemModifier').html(attribute+''+defaultModifier);
    });
  }
  else {
    skillsLevelsArray= JSON.parse(skillsLevelsArray);

    $.each(skillsLevelsArray, function(i, value) {
      var levelInput = $('#skillId'+value.skillId);
      var checkbox = levelInput.parent().parent().find('.skillItemCheckbox');
      var attribute = checkbox.attr('data-skill-attribute');
      var difficulty = checkbox.attr('data-skill-difficulty');
      var modifier=checkbox.attr('data-skill-default');
      levelInput.val(value.skillLevel);

      $.each(skillLevelCostsTable, function(i, v) {
        if(v.difficulty==difficulty&&v.level==levelInput.val())
        {
          modifier=v.modifier;
          return;
        }
      });
      if(modifier>=0)
      {
        modifier='+'+modifier;
      }
      levelInput.parent().find('.skillItemModifier').html(attribute+''+modifier);


    });

  }
  $('#skillsTotalCostsSpan').html($('#character_skillsCosts').val()+'cp');
}

function saveSkillsLevels()
{
  var skillsLevelsArray={};
  var counter=0;
  $('#skills .skillItem').each(function saveEachSkillLevels(){
    var toto=$(this);
    var checkbox = toto.find('.skillItemCheckbox');
    var level = toto.find('.skillItemLevel');
    if (level == "")
    {
      level=0;
    }
    var roll = toto.find('.skillItemModifier');
    var label = toto.find('label');
    skillsLevelsArray[counter]={'skillId':checkbox.attr('data-skill-id'),'skillLevel':level.val(),'skillAttribute':checkbox.attr('data-skill-attribute'),'skillDefault':checkbox.attr('data-skill-default'),'skillRoll':roll.html(),'skillName':label.html()};
    counter=counter+1;
  });
  $('#character_skillsLevels').val(JSON.stringify(skillsLevelsArray));
}

function findSkillLevel(level,difficulty)
{
  var output ={};
  $.each(skillLevelCostsTable, function(i, v) {
    if(v.difficulty==difficulty&&parseInt(v.level)==parseInt(level))
    {

      output = {'modifier':v.modifier,'cost':v.cost*-1};
      return;
    }
  });
  return output;
}
/*
function saveInventory()
{
  var inventoryArray={};
  var counter=0;
  $('#itemInventory .inventoryItem').each(function saveEachInventoryItem(){
    var toto=$(this);
    var count = toto.find('input');
    if(count.val()>0)
    {
      inventoryArray[counter]={'id':toto.attr('data-item-id'),'count':count.val(),'label':toto.attr('data-item-label'),'weight':toto.attr('data-item-weight')};
      counter=counter+1;
    }
  });
  $('#character_inventory').val(JSON.stringify(inventoryArray));

  var inventoryWArray={};
  var counter=0;
  $('#firearmInventory .inventoryFirearm').each(function saveEachInventoryFirearm(){
    var toto=$(this);
    var count = toto.find('input');
    if(count.val()>0)
    {
      inventoryWArray[counter]={'id':toto.attr('data-firearm-id'),'count':count.val(),'label':toto.attr('data-firearm-label'),'weight':toto.attr('data-firearm-weight')};
      counter=counter+1;
    }
  });
  $('#character_firearmInventory').val(JSON.stringify(inventoryWArray));

  var inventoryWArray={};
  var counter=0;
  $('#wearableInventory .inventoryWearable').each(function saveEachInventoryWearable(){
    var toto=$(this);
    var count = toto.find('input');
    if(count.val()>0)
    {
      inventoryWArray[counter]={'id':toto.attr('data-wearable-id'),'count':count.val(),'label':toto.attr('data-wearable-label'),'weight':toto.attr('data-wearable-weight'),'slots':toto.attr('data-wearable-slots')};
      counter=counter+1;
    }
  });
  $('#character_meeleeInventory').val(JSON.stringify(inventoryWArray));
  var inventoryWArray={};
  var counter=0;
  $('#meeleeInventory .inventoryMeelee').each(function saveEachInventoryMeelee(){
    var toto=$(this);
    var count = toto.find('input');
    if(count.val()>0)
    {
      inventoryWArray[counter]={'id':toto.attr('data-meelee-id'),'count':count.val(),'label':toto.attr('data-meelee-label'),'weight':toto.attr('data-meelee-weight'),'slots':toto.attr('data-meelee-slots')};
      counter=counter+1;
    }
  });
  $('#character_meeleeInventory').val(JSON.stringify(inventoryWArray));
}
*/
function loadInventory()
{
  var inventory = $('#character_itemInventory');
  var inventoryArray = inventory.val();
  if (inventoryArray != '')
  {
    inventoryArray= JSON.parse(inventoryArray);

    $.each(inventoryArray, function(i, value) {
      appendItemToInventory(value);
    });
  }

  var inventoryW = $('#character_firearmInventory');
  var inventoryWArray = inventoryW.val();
  if (inventoryWArray != '')
  {
    inventoryWArray= JSON.parse(inventoryWArray);

    $.each(inventoryWArray, function(i, value) {
      appendFirearmToInventory(value);
    });
  }
  var inventoryW = $('#character_wearableInventory');
  var inventoryWArray = inventoryW.val();
  if (inventoryWArray != '')
  {
    inventoryWArray= JSON.parse(inventoryWArray);

    $.each(inventoryWArray, function(i, value) {
      appendWearableToInventory(value);
    });
  }
  var inventoryW = $('#character_meeleeInventory');
  var inventoryWArray = inventoryW.val();
  if (inventoryWArray != '')
  {
    inventoryWArray= JSON.parse(inventoryWArray);

    $.each(inventoryWArray, function(i, value) {
      appendMeeleeToInventory(value);
    });
  }

  var inputValue = $('#character_equip').val();
  if(inputValue!="")
  {
    var json=JSON.parse(inputValue);
    if(json.weapon.id!=undefined)
    {
      var element;
      if(json.weapon.type=='firearm')
      {
        element=getElement('firearm',json.weapon);
      }
      if(json.weapon.type=='meelee')
      {
        element=getElement('meelee',json.weapon);
      }
      $('#equipedWeapon').html(element);
    }
    if(json.wearable.list.length>0)
    {
      $.each(json.wearable.list, function(i, v) {
      var element=getElement('wearable',v);
      $('#equipedWearable').append(element);
      });
    }
  }

}
// get prepared element for inventory items
function getElement(type,object)
{
  if(type=='wearable')
  {
    return '<span data-wearable-damageResist="'+object.damageResist+'" data-wearable-energyResist="'+object.energyResist+'" data-wearable-radiationResist="'+object.radiationResist+'" data-wearable-id="'+object.id+'">'+object.name+'</span>';
  }
  if(type=='firearm')
  {
    return '<span data-firearm-id="'+object.id+'">'+object.name+'</span>';
  }
  if(type=='meelee')
  {
    return '<span data-meelee-id="'+object.id+'">'+object.name+'</span>';
  }
}
function appendItemToInventory(value)
{
  $('#itemInventory .box-scrollable').append('<div class="inventoryItem" data-item-id="'+value.id+'" data-item-weight="'+value.weight+'" data-item-label="'+value.label+'"><label>'+value.label+'</label><input type="number" class="mainAttributes_field" value="'+value.count+'"></div>');
}
function appendFirearmToInventory(value)
{
  $('#firearmInventory .box-scrollable').append('<div class="inventoryFirearm" data-firearm-id="'+value.id+'" data-firearm-weight="'+value.weight+'" data-firearm-label="'+value.label+'"><label>'+value.label+'</label><input type="number" class="mainAttributes_field" value="'+value.count+'"></div>');
}
function appendMeeleeToInventory(value)
{
  $('#meeleeInventory .box-scrollable').append('<div class="inventoryMeelee" data-meelee-id="'+value.id+'" data-meelee-weight="'+value.weight+'" data-meelee-label="'+value.label+'"><label>'+value.label+'</label><input type="number" class="mainAttributes_field" value="'+value.count+'"></div>');
}
function appendWearableToInventory(value)
{
  $('#wearableInventory .box-scrollable').append('<div class="inventoryWearable" data-wearable-id="'+value.id+'" data-wearable-slots="'+value.slots+'" data-wearable-weight="'+value.weight+'" data-wearable-label="'+value.label+'"><label>'+value.label+'</label><input type="number" class="mainAttributes_field" value="'+value.count+'"></div>');
}

function convertTextToNumberInputs()
{
  $('.mainAttributes_field:not(#skills .mainAttributes_field), .mainAttributes_field_modifier,.idCard_field_number,.idCard_field_number_long').each(function eachNumberInput(){
    var toto=$(this);
    toto.attr('type','number');
  });
}
function disableCalculatedFields()
{
  $('.mainAttributes_field_generated').each(function eachCalculatedField(){
    var toto=$(this);
    //toto.attr('disabled','disabled');
    toto.attr('autocomplete','off');
  });
}
function inputDynamicWidth()
{
  $('.mainAttributes_field,.mainAttributes_field_modifier,.mainAttributes_field_generated').not("span.mainAttributes_field_generated").each(function(){
    var toto = $(this);
    var width = toto.val().length;
    toto.css('width',((width + 1) * 9) + 'px') ;
  });
}
function appendEquippedFirearm(element)
{
  var id = $(element).attr('data-firearm-id');
  var data = {};
  var type = 'firearm';
  data[type]=id;
  $.ajax({
            url: '/api',
            type: "GET",
            data: data,
            success: function(result) {
              var json=JSON.parse(result);
                updateEquipArray('firearm',json);
                var element=getElement('firearm',json);
                $('#equipedWeapon').html(element);
            }
    });
}
function appendEquippedMeelee(element)
{
  var id = $(element).attr('data-meelee-id');
  var data = {};
  var type = 'meelee';
  data[type]=id;
  $.ajax({
            url: '/api',
            type: "GET",
            data: data,
            success: function(result) {
              var json=JSON.parse(result);
                updateEquipArray('meelee',json);
                var element=getElement('meelee',json);
                $('#equipedWeapon').html(element);
            }
    });
}
function appendEquippedWearable(element)
{
  var id = $(element).parent().attr('data-wearable-id');
  var data = {};
  var type = 'wearable';
  data[type]=id;
  $.ajax({
            url: '/api',
            type: "GET",
            data: data,
            success: function(result) {
                var json=JSON.parse(result);
                updateEquipArray('wearable',json);
                //var element=getElement('wearable',json);
                //$('#equipedWearable').append(element);
            }
    });
}
function unequipWearable(id,equipArray)
{
  $.each(equipArray.wearable.list, function(i, v) {

    if(v==undefined||id==v.id)
    {
      equipArray.wearable.list.splice(i, 1);
    }
    if(equipArray.wearable.head.id==id)
    {
      equipArray.wearable.head.id='';
      equipArray.wearable.head.damageResist=0;
    }
    if(equipArray.wearable.face.id==id)
    {
      equipArray.wearable.face.id='';
      equipArray.wearable.face.damageResist=0;
    }
    if(equipArray.wearable.eyes.id==id)
    {
      equipArray.wearable.eyes.id='';
      equipArray.wearable.eyes.damageResist=0;
    }
    if(equipArray.wearable.chest.id==id)
    {
      equipArray.wearable.chest.id='';
      equipArray.wearable.chest.damageResist=0;
    }
    if(equipArray.wearable.armL.id==id)
    {
      equipArray.wearable.armL.id='';
      equipArray.wearable.armL.damageResist=0;
    }
    if(equipArray.wearable.armR.id==id)
    {
      equipArray.wearable.armR.id='';
      equipArray.wearable.armR.damageResist=0;
    }
    if(equipArray.wearable.legR.id==id)
    {
      equipArray.wearable.legR.id='';
      equipArray.wearable.legR.damageResist=0;
    }
    if(equipArray.wearable.legL.id==id)
    {
      equipArray.wearable.legL.id='';
      equipArray.wearable.legL.damageResist=0;
    }
  });
  return equipArray;
}
function updateEquipArray(type,object)
{
  var equipInput = $('#character_equip');
  var equipArray = equipInput.val();
  if(equipArray=="")
  {
    equipArray=equipArrayEmpty;
  }
  else {
    equipArray = JSON.parse(equipInput.val());
  }
  if(type=='firearm')
  {
    object['type']='firearm';
    equipArray.weapon=object;
  }
  if(type=='melee')
  {
    object['type']='meelee';
    equipArray.weapon=object;
  }
  if(type=='wearable')
  {
    var skip = 0;
    $.each(equipArray.wearable.list, function(i, v) {
      if(v.id == object.id)
      {
        skip = 1;
      }
    });
    if(skip==0)
    {

      var toremove=[];
      $.each(object.slots, function(i, v) {
        if(equipArray.wearable[v].id>0 && $.inArray(equipArray.wearable[v].id, toremove) == -1)
        {
          toremove.push(equipArray.wearable[v].id);
        }
        equipArray.wearable[v].id=object.id;
        equipArray.wearable[v].damageResist=object.damageResist;
        if(v=='head')
        {
          $('#character_damageResistanceHead').val(object.damageResist);
        }
        else if(v=='face')
        {
          $('#character_damageResistanceFace').val(object.damageResist);
        }
        else if(v=='eyes')
        {
          $('#character_damageResistanceEyes').val(object.damageResist);
        }
        else if(v=='armR')
        {
          $('#character_damageResistanceRightArm').val(object.damageResist);
        }
        else if(v=='armL')
        {
          $('#character_damageResistanceLeftArm').val(object.damageResist);
        }
        else if(v=='legR')
        {
          $('#character_damageResistanceRightLeg').val(object.damageResist);
        }
        else if(v=='legL')
        {
          $('#character_damageResistanceLeftLeg').val(object.damageResist);
        }
        else if(v=='chest')
        {
          $('#character_damageResistanceBody').val(object.damageResist);
        }

      });
      $.each(toremove, function(i, v) {
      equipArray=unequipWearable(v,equipArray);
      });
      equipArray.wearable.list.push(object);
      $('#equipedWearable').empty();
      $.each(equipArray.wearable.list, function(i, v) {
      var element=getElement('wearable',v);
      $('#equipedWearable').append(element);
      });
    }
  }
  equipInput.val(JSON.stringify(equipArray));
}

function loadBuyableEffects(target)
{
  var json=$('#character_effects').val();
  if(json=='')
  {
    json={'buyable':[],'game':[]};
  }
  else {
    json  = JSON.parse(json);
  }
  output  = [];
  $('.addQuirk, .selectSkillLevel, .addSkill').show();
  $.each(json.buyable,function loadBuyableEffectsEach(i,v){
    if(v.class=='quirk')
    {
      output.push('<div data-cost="'+v.cost+'" data-class="'+v.class+'" data-id="'+v.id+'" data-name="'+v.name+'" data-effect="'+v.effect+'" class="sell-item">'+v.name+' '+v.level+'lvl  '+v.cost+'cp</div>');
      $('#quirk-box-'+v.id+' .addQuirk').hide();
    }
    if(v.class=='skill')
    {
      output.push('<div data-cost="'+v.cost+'" data-class="'+v.class+'" data-id="'+v.id+'" data-name="'+v.name+'" data-effect="'+v.effect+'" class="sell-item">'+v.name+'  '+v.level+'lvl  '+v.cost+'cp</div>');
      $('#skill-box-'+v.id+' .addSkill, #skill-box-'+v.id+' .selectSkillLevel').hide();
    }
  });
  $(target).html(output.join(''));
}

function addBuyableEffects(neweffects)
{
  var json=$('#character_effects').val();
  if(json=='')
  {
    json={'buyable':[],'game':[]};
  }
  else {
    json  = JSON.parse(json);
  }

  $.each(neweffects,function addBuyableEffectsEach(i,v){
    var item = $(v);

    if (item.attr('data-class')=='quirk')
    {
      json.buyable.push({'name':item.attr('data-name'),'id':item.attr('data-id'),'class':item.attr('data-class'),'cost':item.attr('data-cost'),'effect':item.attr('data-effect'),'level':item.attr('data-level')});
    }
    if (item.attr('data-class')=='skill')
    {
      json.buyable.push({'name':item.attr('data-name'),'id':item.attr('data-id'),'class':item.attr('data-class'),'attribute':item.attr('data-attribute'),'modifier':item.attr('data-modifier'),'cost':item.attr('data-cost'),'level':item.attr('data-level'),'effect':item.attr('data-effect')});

    }
  });
  $('#character_effects').val(JSON.stringify(json));
  $('#skillShop_buy').html('');
}
function removeBuyableEffects(effectstoremove)
{
  var json=$('#character_effects').val();
  if(json=='')
  {
    json={'buyable':[],'game':[]};
  }
  else {
    json  = JSON.parse(json);
  }
  $.each(effectstoremove,function removeBuyableEffectsEach(i,v){
    var item = $(v);
    $.each(json.buyable,function effectsEach(ii,vv){
      if(vv != undefined && item.attr('data-class') == vv.class && item.attr('data-id') == vv.id)
      {
        json.buyable.splice(ii, 1);
        return;
      }
    });

  });

  $('#character_effects').val(JSON.stringify(json));
  $('#skillShop_sell').html('');
}

function setSkillShopTransactionCost(target,elements){
  var transactionCost = 0;
  var cost=0;
  $.each(elements,function(i,v){
    cost = parseInt($(v).attr('data-cost'));
    if($(v).is('.toSell'))
    {
      transactionCost -= cost;
    }
    else {
      transactionCost += cost;
    }

  });
  target.html(transactionCost);
}

function countInEffects()
{
  var reaction=0;
  var sum = {};
  var sumCount=0;
    $('.effectsAll input').each(function(){
      if($(this).val() != "")
      {
        var json = JSON.parse($(this).val());
        if(json.type=='attribute')
        {
          var current = parseInt($('#character_'+json.variable).val());
          var level = parseInt($(this).attr('data-level'));
          if(sum[json.variable]==undefined)
          {
            sum[json.variable]=0;
          }
          sum[json.variable]+=json.modifier*level;
        }
        if(json.type=='reaction')
        {
          var level = parseInt($(this).attr('data-level'));
          if(sum['reaction']==undefined)
          {
            sum['reaction']=0;
          }
          sum['reaction']+=json.modifier*level;
        }
      }

    });
    $.each(appearanceLevelTable,function(index,value){
      if(value["level"]==$('#character_appearance').val())
      {
        if(sum['reaction']==undefined)
        {
          sum['reaction']=0;
        }
        sum['reaction']+=value["modifier"];
        return;
      }
    });
    var previous = 0;
    $.each(sizeModifierTable,function(index,value){
      previous = 1;
      if(index==0)
      {
        previous = 0;
      }
      if(value["size"]==parseFloat($('#character_height').val()))
      {
        if(sum['size']==undefined)
        {
          sum['size']=0;
        }
        sum['size']+=value["modifier"];
        return;
      }
      else {
        if(value["size"]>parseFloat($('#character_height').val())&&sizeModifierTable[index-previous]["size"]<parseFloat($('#character_height').val()))
        {
          if(sum['size']==undefined)
          {
            sum['size']=0;
          }
          sum['size']+=value["modifier"];

          return;
        }
      }

    });
    $.each(sum,function(i,v){

      if(i=='reaction')
      {
        $('#reactionModifierRelative').html(v);
        $('#character_reactionModifier').val(v);
      }
      if(i=='size')
      {
        $('#sizeModifierRelative').html(v);
        $('#character_sizeModifier').val(v);
      }
      else {
        $('#character_'+i+'Modifier').val(v);
        v+=parseInt($('#character_'+i).val());
        $('#'+i+'Relative').html(v);
      }
    });
    $('#mainAttributes .relativeMod').each(function(){
      var val=parseInt($(this).html());

      if(val==0)
      {
        var id = '#character_'+$(this).attr('id').replace('Relative','');
        $(this).html($(id).val());
      }
    });
}

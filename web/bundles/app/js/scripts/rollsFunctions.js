function rollTheDices(count)
{
  var result=[];
  for (var i = 0; i < count; i++) {
    result.push(random(1,6));
  }
  return result;
}
function updateCharacterSuccessRollsSelect(id)
{
  var data = {};
  var type = 'character';
  data[type]=id;
  $.ajax({
            url: '/api',
            type: "GET",
            data: data,
            success: function(result) {
                var json=JSON.parse(result);
                var select=$('#successRollAgainst');
                var st = json.strength+json.strengthModifier,
                    dx = json.dexterity+json.dexterityModifier,
                    iq = json.intelligence+json.intelligenceModifier,
                    ht = json.health+json.healthModifier;
                select.find('optgroup[label="Attributes"]').empty().append(
                  '<option value="'+st+'">ST '+st+'</option>',
                  '<option value="'+dx+'">DX '+dx+'</option>',
                  '<option value="'+iq+'">IQ '+iq+'</option>',
                  '<option value="'+ht+'">HT '+ht+'</option>',
                );
                console.log(json);
            }
    });
}
function updateCharacterDamageRollsSelect(id)
{
  var data = {};
  var type = 'character';
  data[type]=id;
  $.ajax({
            url: '/api',
            type: "GET",
            data: data,
            success: function(result) {
                var json=JSON.parse(result);
                var firearms=JSON.parse(json.firearmInventory);
                var select=$('#damageRollWeapon');
                select=select.find('optgroup[label="Firearm"]');
                select.empty();
                $.each(firearms,function(i,v){
                  select.append(
                    '<option value="'+v.damage+'">'+v.name+' '+v.damage+' '+v.damageType+'</option>'
                  );
                });
                var meelees=JSON.parse(json.meeleeInventory);
                var select=$('#damageRollWeapon');
                select=select.find('optgroup[label="Meelee"]');
                select.empty();
                $.each(meelees,function(i,v){
                  if(v.thrustDamage!="")
                  {
                    select.append(
                      '<option value="'+v.thrustDamage+'">'+v.name+' Thrust:'+v.thrustDamage+'</option>'
                    );
                  }
                  if(v.swingDamage!="")
                  {
                  select.append(
                    '<option value="'+v.swingDamage+'">'+v.name+' Swing: '+v.swingDamage+'</option>'
                  );
                  }
                });


            }
    });
}
function updateCharacterAttackRollsSelect(id)
{
  var data = {};
  var type = 'character';
  data[type]=id;
  $.ajax({
            url: '/api',
            type: "GET",
            data: data,
            success: function(result) {
                var json=JSON.parse(result);
                var firearms=JSON.parse(json.firearmInventory),meelees=JSON.parse(json.meeleeInventory);
                var select=$('#attackRollWeapon,#damageRollWeapon');
                select=select.find('optgroup[label="Firearm"]');
                select.empty();

                $.each(firearms,function(i,v){
                  select.append(
                    '<option value="'+v.damage+'" data-object="">'+v.name+' '+v.damage+' '+v.damageType+'</option>'
                  );
                });

                var select=$('#attackRollWeapon,#damageRollWeapon');
                select=select.find('optgroup[label="Meelee"]');
                select.empty();
                $.each(meelees,function(i,v){
                  if(v.thrustDamage!="")
                  {
                    select.append(
                      '<option value="'+v.thrustDamage+'">'+v.name+' Thrust:'+v.thrustDamage+'</option>'
                    );

                  }
                  if(v.swingDamage!="")
                  {
                  select.append(
                    '<option value="'+v.swingDamage+'">'+v.name+' Swing: '+v.swingDamage+'</option>'
                  );

                  }
                });


            }
    });
}
function updateCharacterReactionRollModifier(id)
{
  var data = {};
  var type = 'character';
  data[type]=id;
  $.ajax({
            url: '/api',
            type: "GET",
            data: data,
            success: function(result) {
                var json=JSON.parse(result);
                var input=$('#reactionRollModifier');
                input.val(json.reactionModifier);
            }
    });
}

function updateCharactersPhoto(id)
{
  var data = {};
  var type = 'character';
  data[type]=id;
  $.ajax({
            url: '/api',
            type: "GET",
            data: data,
            success: function(result) {
                var json=JSON.parse(result);
                var input=$('#rollsTopPhoto');
                input.html('<img src="/uploads/characters/'+json.imageName+'">');
            }
    });
}

function random(min,max)
{
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

function updateCommonSelects()
{
  var newoptions='';
  $.each(hitLocationsTable,function(i,v){
    newoptions+='<option value="'+v.modifier+'">'+v.location+'  '+v.modifier+'</option>';
  });
  $('#attackRollBodyPartModifier').find('optgroup[label="Humanoid"]').append(newoptions);

  newoptions='';
  $.each(damageTypeTable,function(i,v){
    newoptions+='<option value="'+v.multiplier+'">'+v.name+'  '+v.multiplier+'x</option>';
  });
  $('#damageRollTotalDamageMultiplier').append(newoptions);
}

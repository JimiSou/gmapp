$(function onLoadAndListeners()
{
  $( document ).tooltip();
  //loadSkillsLevels();
  convertTextToNumberInputs();
  disableCalculatedFields();
  loadInventory();
  recalculateAll();

  $(document).on('mouseenter','#idCard_photo',function showImageUpload(){
    $('#photo_upload').fadeIn();
  });

  $(document).on('mouseleave','#idCard_photo',function hideImageUpload(){
    $('#photo_upload').hide();
  });

  $(document).on('click','#photo_upload_button',function uploadButtonClick(){
    $('#character_imageFile_file').trigger('click');
  });

  $(document).on('click','#bioButton',function showBioModal(){
    $('#appearanceDescText').toggle();
  });

  $(document).on('mouseover','.box',function showTopBoxControlls(){
    $(this).find('.box-top').show();
  });

  $(document).on('mouseleave','.box',function hideTopBoxControlls(){
    $(this).find('.box-top').fadeOut();
  });

  $('i.icofont-ui-close').on('click',function closeBox(e){
    e.preventDefault();
    var what = $(this).parent().parent().attr('id');
    what = '#toggle' + what.charAt(0).toUpperCase() + what.slice(1);
    $(what).toggleClass('btn-active');
    $(this).parent().parent().fadeOut();
  });

  $('#boxToggles i').on('click',function toggleBoxVisibility(e){
    e.preventDefault();
    var what = $(this).attr('id').replace('toggle', '');
    what = '#'+ what.charAt(0).toLowerCase() + what.slice(1);
    if($(what).css('display') !== 'none')
    {
      $(what).fadeOut();
    }
    else {
      $(what).fadeIn();
    }
    $(this).toggleClass('btn-active');
  });
  var counter=100;
  $('.box').draggable({
    start: function() {
      $(this).css('z-index','999999');
    },
    stop: function() {
      $(this).css('z-index',counter);
      counter+=1;
    },

  });
  $('.box').draggable('disable');

  var toggle=0;
  $(document).on('mousedown','#toggleBoxDrag',function toggleBoxDrag(e){

    if(toggle==0)
    {
      $('.box').draggable('enable');
      $(this).removeClass('icofont-toggle-off').addClass('icofont-toggle-on');
      toggle=1;
    }
    else {
      $('.box').draggable('disable');
      $(this).addClass('icofont-toggle-off').removeClass('icofont-toggle-on');
      toggle=0;
    }
  });
  $( '.box-scrollable' ).on( 'mousewheel DOMMouseScroll', function insideDivScroll( e ) {
    var e0 = e.originalEvent,
        delta = e0.wheelDelta || -e0.detail;

    this.scrollTop += ( delta < 0 ? 1 : -1 ) * 10;
    e.preventDefault();
  });
  $(document).on('mousedown','#saveButton',function submitForm(e){
      recalculateAll();
      //saveSkillsLevels();
      saveInventory();
      $('#character_save').trigger('click');
  });
  $(document).on('click','#recalculateButton',function recalculateButtonClickListener(e){
    recalculateAll();
  });
  $(document).on('click','.inventoryFirearm',function equipFirearmClickListener(e){
    appendEquippedFirearm(this);
  });
  $(document).on('click','.inventoryMeelee',function equipMeeleeClickListener(e){
    appendEquippedMeelee(this);
  });
  $(document).on('click','.inventoryWearable',function equipWearableClickListener(e){
    appendEquippedWearable(this);
  });
  $(document).on('click','.tab',function shopItemClickListener(e){
    var id = $(this).attr('id');
    if(id.indexOf('All')>-1)
    {
      $(this).parent().parent().find('.tabScreen').show();
    }
    else {
      id = '#'+id.replace('Toggle','');
      $(this).parent().parent().find('.tabScreen').hide();
      $(id).show();
    }

    $(this).parent().find('.tab').removeClass('tab-active');
    $(this).addClass('tab-active');
  });
  $(document).on('click','#skillShop_list_tabs .tab',function shopItemClickListener(e){
    e.stopPropagation();
    var id = $(this).attr('id');
    if(id.indexOf('All')>-1)
    {
      $('#skillShop_list_screen .tabScreen').show();
    }
    else {
      id = '#'+id.replace('Toggle','');
      $('#skillShop_list_screen .tabScreen').hide();
      $(id).show();
    }

    $(this).parent().find('.tab').removeClass('tab-active');
    $(this).addClass('tab-active');
  });
  $(document).on('click','.shop-item',function shopItemClickListener(e){
    var id = $(this).attr('id');
    id = '#'+id.replace('toggle','box');
    $('#skillShop_screen div').hide();
    $(id).show();
    $('.shop-item').removeClass('shop-item-active');
    $(this).addClass('shop-item-active');
  });
  $(document).on('click','.addQuirk',function addQuirkClickListener(e){
    var toto =$(this),
        classe=toto.attr('data-class'),
        id=toto.attr('data-id'),
        name=toto.attr('data-name'),
        cost=toto.attr('data-cost'),
        level=toto.attr('data-level'),
        effect= toto.attr('data-effect');
        if(level == 0 || level>1)
        {
          var result = prompt("What level you want?", "1");
              result=parseInt(result);
          if (result !== null || result !==0) {
            cost=cost*result;
            var cost2=cost*-1;
            $('#skillShop_buy').append('<div class="toBuy" data-id="'+id+'" data-class="'+classe+'" data-cost="'+cost+'" data-name="'+name+'" data-level="'+result+'">'+name+' lvl '+result+' '+cost2+' <i class="icofont icofont-delete btn"></i></div>')
          }
        }
        else {
          var cost2=cost*-1;
          $('#skillShop_buy').append('<div class="toBuy" data-id="'+id+'" data-class="'+classe+'" data-cost="'+cost+'" data-name="'+name+'" data-level="1">'+name+' '+cost2+' <i class="icofont icofont-delete btn"></i></div>')
        }
        setSkillShopTransactionCost($('#skillShop_counter_transCp'),$('#skillShop_buy .toBuy,#skillShop_sell .toSell'));
  });
  $(document).on('click','.selectSkillLevel',function addSkillClickListener(e){
    var toto        = $(this),
        difficulty  = toto.attr('data-difficulty'),
        attribute   = toto.attr('data-attribute'),
        select      = toto.next().find('.skillLevelSelect'),
        options     = [];
      $.each(skillLevelCostsTable,function(i,v){
        if(v.difficulty==difficulty)
        {
          var modifier;
          if(v.modifier>=0){modifier='+'+v.modifier;}else{modifier=v.modifier;}
          options.push('<option value="'+v.level+' '+v.cost+' '+v.modifier+'">'+v.level+' lvl '+attribute+''+modifier+' '+v.cost+'cp</option>');
        }
      });
      select.html(options.join(''));
    $(this).hide().next().fadeIn();
  });
  $(document).on('click','.addSkill',function addSkillClickListener(e){
    var toto        = $(this),
        classe      = toto.attr('data-class'),
        id          = toto.attr('data-id'),
        selectVal   = toto.prev().val(),
        name        = toto.attr('data-name'),
        attribute   = toto.attr('data-attribute');

    selectVal   = selectVal.split(' ');

    var level       = selectVal[0],
        cost        = parseInt(selectVal[1]),
        modifier    = parseInt(selectVal[2]);

    $('#skillShop_buy').append('<div class="toBuy" data-id="'+id+'" data-class="'+classe+'" data-attribute="'+attribute+'" data-modifier="'+modifier+'" data-cost="-'+cost+'" data-name="'+name+'" data-level="'+level+'" >'+name+' lvl '+level+' '+cost+' <i class="icofont icofont-delete btn"></i></div>')
    setSkillShopTransactionCost($('#skillShop_counter_transCp'),$('#skillShop_buy .toBuy,#skillShop_sell .toSell'));
  });
  $(document).on('click','#skillShop_buy i.icofont-delete',function removeItemClickListener(e){
    $(this).parent().remove();
    setSkillShopTransactionCost($('#skillShop_counter_transCp'),$('#skillShop_buy .toBuy,#skillShop_sell .toSell'));
  });
  $(document).on('click','#skillShop_sell i.icofont-delete',function removeItemClickListener(e){
    var toto        = $(this).parent(),
        classe      = toto.attr('data-class'),
        id          = toto.attr('data-id'),
        cost        = toto.attr('data-cost'),
        name        = toto.attr('data-name'),
        effect        = toto.attr('data-effect');
    if(classe=='quirk')
    {
      $('#skillShop_myList').append('<div data-cost="'+cost+'" data-class="'+classe+'" data-id="'+id+'" data-name="'+name+'" class="sell-item">'+name+' '+cost+'cp</div>');
    }
    if(classe=='skill')
    {
      $('#skillShop_myList').append('<div data-cost="'+cost+'" data-class="'+classe+'" data-id="'+id+'" data-name="'+name+'" class="sell-item">'+name+'  '+level+'lvl  '+cost+'cp</div>');
    }
    $(this).parent().remove();
    setSkillShopTransactionCost($('#skillShop_counter_transCp'),$('#skillShop_buy .toBuy,#skillShop_sell .toSell'));

  });
  $(document).on('click','#confirmTransaction',function confirmTransactionClickListener(e){
      addBuyableEffects($('#skillShop_buy .toBuy'));
      removeBuyableEffects($('#skillShop_sell .toSell'));
      loadBuyableEffects($('#skillShop_myList'));
      $('#skillShop_counter_transCp').html('0');
      recalculateAll();
  });
  $(document).on('click','.sell-item',function addSkillClickListener(e){
    var toto        = $(this),
        classe      = toto.attr('data-class'),
        id          = toto.attr('data-id'),
        cost        = toto.attr('data-cost'),
        name        = toto.attr('data-name'),
        effect        = toto.attr('data-effect');
    toto.remove();


    $('#skillShop_sell').append('<div class="toSell" data-class="'+classe+'" data-name="'+name+'" data-cost="'+cost+'" data-id="'+id+'">'+name+' '+cost+'cp <i class="icofont icofont-delete btn"></i></div>')
    setSkillShopTransactionCost($('#skillShop_counter_transCp'),$('#skillShop_buy .toBuy,#skillShop_sell .toSell'));
  });
});

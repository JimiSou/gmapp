var handsDamageTable =
[
{'strength':1,'thrust':'1d-6','swing':'1d-5'},
{'strength':2,'thrust':'1d-6','swing':'1d-5'},
{'strength':3,'thrust':'1d-5','swing':'1d-4'},
{'strength':4,'thrust':'1d-5','swing':'1d-4'},
{'strength':5,'thrust':'1d-4','swing':'1d-3'},
{'strength':6,'thrust':'1d-4','swing':'1d-3'},
{'strength':7,'thrust':'1d-3','swing':'1d-2'},
{'strength':8,'thrust':'1d-3','swing':'1d-2'},
{'strength':9,'thrust':'1d-2','swing':'1d-1'},
{'strength':10,'thrust':'1d-2','swing':'1d'},
{'strength':11,'thrust':'1d-1','swing':'1d+1'},
{'strength':12,'thrust':'1d-1','swing':'1d+2'},
{'strength':13,'thrust':'1d','swing':'2d-1'},
{'strength':14,'thrust':'1d','swing':'2d'},
{'strength':15,'thrust':'1d+1','swing':'2d+1'},
{'strength':16,'thrust':'1d+1','swing':'2d+2'},
{'strength':17,'thrust':'1d+2','swing':'3d-1'},
{'strength':18,'thrust':'1d+2','swing':'3d-1'},
{'strength':19,'thrust':'2d-1','swing':'3d+1'},
{'strength':20,'thrust':'2d-1','swing':'3d+2'},
{'strength':21,'thrust':'2d','swing':'4d-1'},
{'strength':22,'thrust':'2d','swing':'4d'},
{'strength':23,'thrust':'2d+1','swing':'4d+1'},
{'strength':24,'thrust':'2d+1','swing':'4d+2'},
{'strength':25,'thrust':'2d+2','swing':'5d-1'},
{'strength':26,'thrust':'2d+2','swing':'5d'},
{'strength':27,'thrust':'3d-1','swing':'5d+1'},
{'strength':28,'thrust':'3d-1','swing':'5d+1'},
{'strength':29,'thrust':'3d','swing':'5d+2'},
{'strength':30,'thrust':'3d','swing':'5d+2'},
{'strength':31,'thrust':'3d+1','swing':'6d-1'},
{'strength':32,'thrust':'3d+1','swing':'6d-1'},
{'strength':33,'thrust':'3d+2','swing':'6d'},
{'strength':34,'thrust':'3d+2','swing':'6d'},
{'strength':35,'thrust':'4d-1','swing':'6d+1'},
{'strength':36,'thrust':'4d-1','swing':'6d+1'},
{'strength':37,'thrust':'4d','swing':'6d+2'},
{'strength':38,'thrust':'4d','swing':'6d+2'},
{'strength':39,'thrust':'4d+1','swing':'7d-1'},
{'strength':40,'thrust':'4d+1','swing':'7d-1'},
{'strength':45,'thrust':'5d','swing':'7d+1'},
{'strength':50,'thrust':'5d+2','swing':'8d-1'},
{'strength':55,'thrust':'6d','swing':'8d+1'},
{'strength':60,'thrust':'7d-1','swing':'9d'},
{'strength':65,'thrust':'7d+1','swing':'9d+2'},
{'strength':70,'thrust':'8d','swing':'10d'},
{'strength':75,'thrust':'8d+2','swing':'10d+2'},
{'strength':80,'thrust':'9d','swing':'11d'},
{'strength':85,'thrust':'9d+2','swing':'11d+2'},
{'strength':90,'thrust':'10d','swing':'12d'},
{'strength':95,'thrust':'10d+2','swing':'12d+2'},
{'strength':100,'thrust':'11d','swing':'13d'}
];



var skillLevelCostsTable =
[
  {'difficulty': 'E','level':1,'cost':1, 'modifier':0},
  {'difficulty': 'E','level':2,'cost':2, 'modifier':1},
  {'difficulty': 'E','level':3,'cost':4, 'modifier':2},
  {'difficulty': 'E','level':4,'cost':8, 'modifier':3},
  {'difficulty': 'E','level':5,'cost':12, 'modifier':4},
  {'difficulty': 'E','level':6,'cost':16, 'modifier':5},
  {'difficulty': 'E','level':7,'cost':20, 'modifier':6},
  {'difficulty': 'E','level':8,'cost':24, 'modifier':7},
  {'difficulty': 'E','level':9,'cost':28, 'modifier':8},
  {'difficulty': 'E','level':10,'cost':32, 'modifier':9},
  {'difficulty': 'E','level':11,'cost':36, 'modifier':10},
  {'difficulty': 'E','level':12,'cost':40, 'modifier':11},
  {'difficulty': 'E','level':13,'cost':44, 'modifier':12},
  {'difficulty': 'E','level':14,'cost':48, 'modifier':13},
  {'difficulty': 'E','level':15,'cost':52, 'modifier':14},
  {'difficulty': 'E','level':16,'cost':56, 'modifier':15},
  {'difficulty': 'E','level':17,'cost':60, 'modifier':16},
  {'difficulty': 'E','level':18,'cost':64, 'modifier':17},
  {'difficulty': 'E','level':19,'cost':68, 'modifier':18},
  {'difficulty': 'E','level':20,'cost':72, 'modifier':19},
  {'difficulty': 'A','level':1,'cost':1, 'modifier':-1},
  {'difficulty': 'A','level':2,'cost':2, 'modifier':0},
  {'difficulty': 'A','level':3,'cost':4, 'modifier':1},
  {'difficulty': 'A','level':4,'cost':8, 'modifier':2},
  {'difficulty': 'A','level':5,'cost':12, 'modifier':3},
  {'difficulty': 'A','level':6,'cost':16, 'modifier':4},
  {'difficulty': 'A','level':7,'cost':20, 'modifier':5},
  {'difficulty': 'A','level':8,'cost':24, 'modifier':6},
  {'difficulty': 'A','level':9,'cost':28, 'modifier':7},
  {'difficulty': 'A','level':10,'cost':32, 'modifier':8},
  {'difficulty': 'A','level':11,'cost':36, 'modifier':9},
  {'difficulty': 'A','level':12,'cost':40, 'modifier':10},
  {'difficulty': 'A','level':13,'cost':44, 'modifier':11},
  {'difficulty': 'A','level':14,'cost':48, 'modifier':12},
  {'difficulty': 'A','level':15,'cost':52, 'modifier':13},
  {'difficulty': 'A','level':16,'cost':56, 'modifier':14},
  {'difficulty': 'A','level':17,'cost':60, 'modifier':15},
  {'difficulty': 'A','level':18,'cost':64, 'modifier':16},
  {'difficulty': 'A','level':19,'cost':68, 'modifier':17},
  {'difficulty': 'A','level':20,'cost':72, 'modifier':18},
  {'difficulty': 'H','level':1,'cost':1, 'modifier':-2},
  {'difficulty': 'H','level':2,'cost':2, 'modifier':-1},
  {'difficulty': 'H','level':3,'cost':4, 'modifier':0},
  {'difficulty': 'H','level':4,'cost':8, 'modifier':1},
  {'difficulty': 'H','level':5,'cost':12, 'modifier':2},
  {'difficulty': 'H','level':6,'cost':16, 'modifier':3},
  {'difficulty': 'H','level':7,'cost':20, 'modifier':4},
  {'difficulty': 'H','level':8,'cost':24, 'modifier':5},
  {'difficulty': 'H','level':9,'cost':28, 'modifier':6},
  {'difficulty': 'H','level':10,'cost':32, 'modifier':7},
  {'difficulty': 'H','level':11,'cost':36, 'modifier':8},
  {'difficulty': 'H','level':12,'cost':40, 'modifier':9},
  {'difficulty': 'H','level':13,'cost':44, 'modifier':10},
  {'difficulty': 'H','level':14,'cost':48, 'modifier':11},
  {'difficulty': 'H','level':15,'cost':52, 'modifier':12},
  {'difficulty': 'H','level':16,'cost':56, 'modifier':13},
  {'difficulty': 'H','level':17,'cost':60, 'modifier':14},
  {'difficulty': 'H','level':18,'cost':64, 'modifier':15},
  {'difficulty': 'H','level':19,'cost':68, 'modifier':16},
  {'difficulty': 'H','level':20,'cost':72, 'modifier':17}

];
var encumbranceLevelTable =
[
{'level':0,'dodge':0,'basicMove':1,'label':'None','basicLift':0},
{'level':1,'dodge':-1,'basicMove':0.8,'label':'Light','basicLift':2},
{'level':2,'dodge':-2,'basicMove':0.6,'label':'Medium','basicLift':3},
{'level':3,'dodge':-3,'basicMove':0.4,'label':'Heavy','basicLift':6},
{'level':4,'dodge':-4,'basicMove':0.2,'label':'Extra-Heavy','basicLift':100}
];

var reactionLevelTable=
[
  {'level':0,'name':'Disastrous','description':'The NPC hates the characters and will act in their worst interest. Nothing is out of the question: assault, betrayal, public ridicule, or ignoring a life-or-death plea are all possible.'},
  {'level':1,'name':'Very Bad','description':'The NPC dislikes the characters and will act against them if it’s convenient to do so: attacking, offering grossly unfair terms in a transaction, and so on.'},
  {'level':2,'name':'Bad','description':'The NPC cares nothing for the characters and will act against them (as above), if he can profit by doing so.'},
  {'level':3,'name':'Poor','description':'The NPC is unimpressed. He may make threats, demand a huge bribe before offering aid, or something similar.'},
  {'level':4,'name':'Neutral','description':'The NPC ignores the characters as much as possible. He is totally uninterested. Transactions will go smoothly and routinely, as long as protocol is observed.'},
  {'level':5,'name':'Good','description':'The NPC likes the characters and will be helpful within normal, everyday limits. Reasonable requests will be granted.'},
  {'level':6,'name':'Very Good','description':'The NPC thinks highly of the characters and will be quite helpful and friendly, freely offering aid and favorable terms in most things.'},
  {'level':7,'name':'Excellent','description':'The NPC is extremely impressed by the characters, and will act in their best interests at all times, within the limits of his own ability – perhaps even risking his life, wealth, or reputation.'}
];

var basicAttributeLevelTable=
[
  {'level':6,'name':'Crippling','description':'An attribute this bad severely constrains your lifestyle.'},
  {'level':7,'name':'Poor','description':'Your limitations are immediately obvious to anyone who meets you. This is the lowest score you can have and still pass for “able-bodied.”'},
  {'level':8,'name':'Below Average','description':'Such scores are limiting, but within the human norm. The GM may forbid attributes below 8 to active adventurers.'},
  {'level':9,'name':'Below Average','description':'Such scores are limiting, but within the human norm. The GM may forbid attributes below 8 to active adventurers.'},
  {'level':10,'name':'Average','description':'Most humans get by just fine with a score of 10!'},
  {'level':11,'name':'Above Average','description':'These scores are superior, but within the human norm.'},
  {'level':12,'name':'Above Average','description':'These scores are superior, but within the human norm.'},
  {'level':13,'name':'Exceptional','description':'Such an attribute is immediately apparent – as bulging muscles, feline grace, witty dialog, or glowing health – to those who meet you.'},
  {'level':14,'name':'Exceptional','description':'Such an attribute is immediately apparent – as bulging muscles, feline grace, witty dialog, or glowing health – to those who meet you.'},
  {'level':15,'name':'Amazing','description':'An attribute this high draws constant comment and probably guides your career choices.'}
];

var appearanceLevelTable=
[
  {'level':0,'name':'Hideous','description':'You have any sort of disgusting looks you can come up with: a severe skin disease, wall-eye . . . preferably several things at once. This gives -4 on reaction rolls.','cost':16,'modifier':-4},
  {'level':1,'name':'Ugly','description':'As Hideous, but not so bad – maybe only stringy hair and snaggle teeth. This gives -2 on reaction rolls.','cost':8,'modifier':-2},
  {'level':2,'name':'Unattractive','description':'You look vaguely unappealing, but it’s nothing anyone can put a finger on. This gives -1 on reaction rolls.','cost':4,'modifier':-1},
  {'level':3,'name':'Average','description':'Your appearance gives you no reaction modifiers either way; you can blend easily into a crowd. A viewer’s impression of your looks depends on your behavior. If you smile and act friendly, you will be remembered as pleasant-looking; if you frown and mutter, you will be remembered as unattractive.','cost':0,'modifier':0},
  {'level':4,'name':'Attractive','description':'You don’t enter beauty contests, but are definitely goodlooking. This gives +1 on reaction rolls.','cost':-4,'modifier':1},
  {'level':5,'name':'Beautiful','description':'You could enter beauty contests. This gives +4 on reaction rolls made by those attracted to members of your sex, +2 from everyone else.','cost':-12,'modifier':2},
  {'level':6,'name':'Very Beautiful','description':'You could win beauty contests regularly. This gives +6 on reaction rolls made by those attracted to members of your sex, +2 from others. Exception: Members of the same sex with reason to dislike you (more than -4 in reaction penalties, regardless of bonuses) resent your good looks, and react at -2 instead. As well, talent scouts, friendly drunks, slave traders, and other nuisances are liable to become a problem for you.','cost':-16,'modifier':6}
];

var equipArrayEmpty={'weapon':{},'wearable': {
'head':{'id':'','damageResist':'','energyResist':'0','radiationResist':'0'},
'chest':{'id':'','damageResist':'','energyResist':'0','radiationResist':'0'},
'eyes':{'id':'','damageResist':'','energyResist':'0','radiationResist':'0'},
'face':{'id':'','damageResist':'','energyResist':'0','radiationResist':'0'},
'armL':{'id':'','damageResist':'','energyResist':'0','radiationResist':'0'},
'armR':{'id':'','damageResist':'','energyResist':'0','radiationResist':'0'},
'legL':{'id':'','damageResist':'','energyResist':'0','radiationResist':'0'},
'legR':{'id':'','damageResist':'','energyResist':'0','radiationResist':'0'},
'list':[]}};

var sizeModifierTable=[
 {"size": 0.05, "modifier": -10},
 {"size": 0.07, "modifier": -9},
 {"size": 0.1, "modifier": -8},
 {"size": 0.15, "modifier": -7},
 {"size": 0.2, "modifier": -6},
 {"size": 0.3, "modifier": -5},
 {"size": 0.5, "modifier": -4},
 {"size": 0.7, "modifier": -3},
 {"size": 1, "modifier": -2},
 {"size": 1.5, "modifier": -1},
 {"size": 2, "modifier": 0},
 {"size": 3, "modifier": 1},
 {"size": 5, "modifier": 2},
 {"size": 7, "modifier": 3},
 {"size": 10, "modifier": 4},
 {"size": 15, "modifier": 5},
 {"size": 20, "modifier": 6},
 {"size": 30, "modifier": 7},
 {"size": 50, "modifier": 8},
 {"size": 70, "modifier": 9},
 {"size": 100, "modifier": 10},
 {"size": 150, "modifier": 11}
];

var damageTypeTable=[
  {
    "name": "Piercing",
    "nameShort": "pi",
    "multiplier": 1,
    "description": "just means that there is no damage multiplier exactly as crushing"
  },
  {
    "name": "Large Piercing +",
    "nameShort": "pi+",
    "multiplier": 1.5,
    "description": "means that there is a x 1.5 multiplier exactly as cutting. The weapon doesn't cut. It just do bigger holes than an ordinary gun."
  },
  {
    "name": "Huge Piercing ++",
    "nameShort": "pi++",
    "multiplier": 2,
    "description": "means that there is a x 2 multiplier exactly as impaling. The weapon doesn't impale in GURPS terms (even if it does in reality – as every bullet actually) but it does very very big holes due to a huge caliber..."
  },
  {
    "name": "Small Piercing -",
    "nameShort": "pi-",
    "multiplier": 0.5,
    "description": "means that there is a x 0.5 multiplier. The caliber is very small and"
  },
  {
    "name": "Crushing",
    "nameShort": "cr",
    "multiplier": 1,
    "description": "-"
  },
  {
    "name": "Burning",
    "nameShort": "burn",
    "multiplier": 1,
    "description": "-"
  },
  {
    "name": "Corrosion",
    "nameShort": "cor",
    "multiplier": 1,
    "description": "-"
  },
  {
    "name": "Fatigue",
    "nameShort": "fat",
    "multiplier": 1,
    "description": "-"
  },
  {
    "name": "Toxic",
    "nameShort": "tox",
    "multiplier": 1,
    "description": "-"
  },
  {
    "name": "Cutting",
    "nameShort": "cut",
    "multiplier": 1.5,
    "description": "-"
  },
  {
    "name": "Impaling",
    "nameShort": "imp",
    "multiplier": 2,
    "description": "-"
  }
];
var hitLocationsTable=[
  {
    "location": "Torso",
    "modifier": 0,
    "description": "The normal and default target."
  },
  {
    "location": "Vitals",
    "modifier": -3,
    "description": "Only targettable by impaling all piercing attacks and tight-beam burning attacks. The damage modifier for impaling or piercing is x4 while tight-beam burning attacks do x2."
  },
  {
    "location": "Skull",
    "modifier": -7,
    "description": "The skull benefits from natural DR 2. The wounding modifier increases to x4 for all attacks. Knockdown rolls are at -10 and use the head-blow table for critical hits."
  },
  {
    "location": "Eye",
    "modifier": -9,
    "description": "Only targettable by impaling and all piercing attacks and tight-beam burning attacks. The same as a skull hit without the extra DR! Injury over HT/10 inflicts a crippling wound."
  },
  {
    "location": "Face",
    "modifier": -5,
    "description": "Sometimes ignores DR if helmet is open faced. Corrosion damage gets a 1.5 wounding modifier blinds one eye and inflicts a major wound. Knockdown rolls at -5 and use the head-blow table for critical hits."
  },
  {
    "location": "Neck",
    "modifier": -5,
    "description": "Crushing and corrosion attacks do x1.5 damage and cutting damage does x2 damage. Anyone killed by a cutting blow to the head has been decapitated!"
  },
  {
    "location": "Groin",
    "modifier": -3,
    "description": "Human males take double shock penalities from crushing damage and get -5 to knockback rolls."
  },
  {
    "location": "Arm",
    "modifier": -2,
    "description": "Large piercing huge piercing and impaling damage is reduced to x1. Any major wound that inflicts over HT/2 damage cripples the limb but after the arm has been crippled any further damage to it is ignored."
  },
  {
    "location": "Leg",
    "modifier": -2,
    "description": "Large piercing huge piercing and impaling damage is reduced to x1. Any major wound that inflicts over HT/2 damage cripples the limb but after the arm has been crippled any further damage to it is ignored."
  },
  {
    "location": "Hands",
    "modifier": -4,
    "description": "As per an Arm or leg but it only requires HT/3 damage to cripple. The penality to hit a hand holding a shield is -8."
  },
  {
    "location": "Feet",
    "modifier": -4,
    "description": "As per an Arm or leg but it only requires HT/3 damage to cripple. The penality to hit a hand holding a shield is -8."
  },
  {
    "location": "Weapon with Reach C",
    "modifier": -5,
    "description": "-"
  },
  {
    "location": "Weapon with Reach 1",
    "modifier": -4,
    "description": "-"
  },
  {
    "location": "Weapon with Reach 2+",
    "modifier": -3,
    "description": "-"
  }
];

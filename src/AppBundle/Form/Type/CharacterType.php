<?php
namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Vich\UploaderBundle\Form\Type\VichImageType;

class CharacterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, array('label'=>'Name:'))
            ->add('alias', TextType::class, array('label'=>'Alias:'))
            ->add('player', TextType::class, array('label'=>'Player:'))
            ->add('age', NumberType::class, array('label'=>'Age:'))
            ->add('race', TextType::class, array('label'=>'Race:'))
            ->add('appearanceDesc', TextareaType::class, array('label'=>'Description:'))
            ->add('appearance', NumberType::class, array('label'=>'Appearance lvl:'))
            ->add('height', NumberType::class, array('label'=>'Height:'))
            ->add('strength', NumberType::class, array('label'=>'ST'))
            ->add('strengthModifier', NumberType::class, array('label'=>'mod:'))
            ->add('dexterity', NumberType::class, array('label'=>'DX'))
            ->add('dexterityModifier', NumberType::class, array('label'=>'mod:'))
            ->add('intelligence', NumberType::class, array('label'=>'IQ'))
            ->add('intelligenceModifier', NumberType::class, array('label'=>'mod:'))
            ->add('health', NumberType::class, array('label'=>'HT'))
            ->add('healthModifier', NumberType::class, array('label'=>'mod:'))
            ->add('hitPoints', NumberType::class, array('label'=>'HP'))
            ->add('hitPointsModifier', NumberType::class, array('label'=>'mod:'))
            ->add('hitPointsCurrent', NumberType::class, array('label'=>'curr:'))
            ->add('will', NumberType::class, array('label'=>'WILL'))
            ->add('willModifier', NumberType::class, array('label'=>'mod:'))
            ->add('perception', NumberType::class, array('label'=>'PER'))
            ->add('perceptionModifier', NumberType::class, array('label'=>'mod:'))
            ->add('fatiguePoints', NumberType::class, array('label'=>'FP'))
            ->add('fatiguePointsModifier', NumberType::class, array('label'=>'mod:'))
            ->add('fatiguePointsCurrent', NumberType::class, array('label'=>'curr:'))
            ->add('basicLift', NumberType::class, array('label'=>'Basic Lift'))
            ->add('basicSpeed', NumberType::class, array('label'=>'Basic Speed'))
            ->add('basicMove', NumberType::class, array('label'=>'Basic Move'))
            ->add('hiking', NumberType::class, array('label'=>'Hiking'))
            ->add('gainedCP', NumberType::class, array('label'=>'Gained'))
            ->add('startingCP', NumberType::class, array('label'=>'Starting'))
            ->add('unusedCP', NumberType::class, array('label'=>'Unused'))
            ->add('damageThrust', TextType::class, array('label'=>'Thrust'))
            ->add('damageSwing', TextType::class, array('label'=>'Swing'))
            ->add('damageResistanceHead', NumberType::class, array('label'=>'Head:'))
            ->add('damageResistanceBody', NumberType::class, array('label'=>'Body:'))
            ->add('damageResistanceLeftLeg', NumberType::class, array('label'=>'LL:'))
            ->add('damageResistanceRightLeg', NumberType::class, array('label'=>'RL:'))
            ->add('damageResistanceLeftArm', NumberType::class, array('label'=>'LA:'))
            ->add('damageResistanceRightArm', NumberType::class, array('label'=>'RA:'))
            ->add('damageResistanceFace', NumberType::class, array('label'=>'Face:'))
            ->add('damageResistanceEyes', NumberType::class, array('label'=>'Eyes:'))
            ->add('energyResistanceHead', NumberType::class, array('label'=>'Head:'))
            ->add('energyResistanceBody', NumberType::class, array('label'=>'Body:'))
            ->add('energyResistanceLeftLeg', NumberType::class, array('label'=>'LL:'))
            ->add('energyResistanceRightLeg', NumberType::class, array('label'=>'RL:'))
            ->add('energyResistanceLeftArm', NumberType::class, array('label'=>'LA:'))
            ->add('energyResistanceRightArm', NumberType::class, array('label'=>'RA:'))
            ->add('energyResistanceFace', NumberType::class, array('label'=>'Face:'))
            ->add('energyResistanceEyes', NumberType::class, array('label'=>'Eyes:'))
            ->add('radiationResistanceHead', NumberType::class, array('label'=>'Head:'))
            ->add('radiationResistanceBody', NumberType::class, array('label'=>'Body:'))
            ->add('radiationResistanceLeftLeg', NumberType::class, array('label'=>'LL:'))
            ->add('radiationResistanceRightLeg', NumberType::class, array('label'=>'RL:'))
            ->add('radiationResistanceLeftArm', NumberType::class, array('label'=>'LA:'))
            ->add('radiationResistanceRightArm', NumberType::class, array('label'=>'RA:'))
            ->add('radiationResistanceFace', NumberType::class, array('label'=>'Face:'))
            ->add('radiationResistanceEyes', NumberType::class, array('label'=>'Eyes:'))
            ->add('effects', HiddenType::class, array())
            ->add('parry', NumberType::class, array('label'=>'Parry'))
            ->add('block', NumberType::class, array('label'=>'Block'))
            ->add('dodge', NumberType::class, array('label'=>'Dodge'))
            ->add('money', NumberType::class, array('label'=>'Money'))
            ->add('itemInventory', HiddenType::class, array())
            ->add('firearmInventory', HiddenType::class, array())
            ->add('wearableInventory', HiddenType::class, array())
            ->add('meeleeInventory', HiddenType::class, array())
            ->add('reactionModifier', HiddenType::class, array())
            ->add('sizeModifier', HiddenType::class, array())
            ->add('equip', HiddenType::class, array())
            ->add('encumbranceLevel', NumberType::class, array('label'=>'Encumbrance'))
            ->add('world', EntityType::class, array(
              'class' => 'AppBundle:World',
              'choice_label' => 'name',
              'multiple'=>true,
              'expanded'=>true))
            ->add('imageFile', VichImageType::class, ['required' => false,'allow_delete' => true])

            ->add('save', SubmitType::class)
        ;
    }
}

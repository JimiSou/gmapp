<?php
namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Vich\UploaderBundle\Form\Type\VichImageType;

class LogType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('type', TextType::class)
            ->add('description', TextareaType::class)
            ->add('roll', TextType::class)
            ->add('rollResult', TextType::class)
            ->add('date', DateTimeType::class,array(
              'widget'=>'text',
              'format'=> 'Y-m-d H:i:s',
              'data' => new \DateTime('2200-03-01 01:01:00'),
            ))
            ->add('character', EntityType::class, array(
              'class' => 'AppBundle:Character',
              'choice_label' => 'name',
              'label' => 'Character',
              'required' => false,))
            ->add('otherCharacter', EntityType::class, array(
              'class' => 'AppBundle:Character',
              'choice_label' => 'name',
              'required' => false,
              'label' =>  'Other Character'))
            ->add('location', EntityType::class, array(
              'class' => 'AppBundle:Location',
              'choice_label' => 'name',
              'required' => false,
              'label' =>  'Location'))
            ->add('skill', EntityType::class, array(
              'class' => 'AppBundle:Skill',
              'choice_label' => 'name',
              'required' => false,
              'label' =>  'Skill'))
            ->add('faction', EntityType::class, array(
              'class' => 'AppBundle:Faction',
              'choice_label' => 'name',
              'required' => false,
              'label' =>  'Faction'))
            ->add('world', EntityType::class, array(
              'class' => 'AppBundle:World',
              'choice_label' => 'name',
              'label' =>  'World'))
            ->add('imageFile', VichImageType::class, ['required' => false,'allow_delete' => true])              
            ->add('save', SubmitType::class)
        ;
    }
}

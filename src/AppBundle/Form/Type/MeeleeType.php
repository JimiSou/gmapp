<?php
namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Vich\UploaderBundle\Form\Type\VichImageType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class MeeleeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class)
            ->add('thrustDamage', TextType::class)
            ->add('thrustReach', TextType::class)
            ->add('swingDamage', TextType::class)
            ->add('swingReach', TextType::class)
            ->add('parry', NumberType::class)
            ->add('cost', NumberType::class)
            ->add('weight', NumberType::class)
            ->add('strength', NumberType::class)
            ->add('notes', TextType::class)
            ->add('technologyLevel', NumberType::class)
            ->add('category', TextType::class)
            ->add('imageFile', VichImageType::class, ['required' => false,'allow_delete' => true])
            ->add('world', EntityType::class, array(
              'class' => 'AppBundle:World',
              'choice_label' => 'name',
              'multiple'=>true,
              'expanded'=>true))
            ->add('save', SubmitType::class)
        ;
    }
}

<?php
namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Vich\UploaderBundle\Form\Type\VichImageType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class FirearmType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class)
            ->add('damage', TextType::class)
            ->add('damageType', TextType::class)
            ->add('accuracy', TextType::class)
            ->add('rangeMin', NumberType::class)
            ->add('rangeMax', NumberType::class)
            ->add('weight', NumberType::class)
            ->add('rateOfFire', TextType::class)
            ->add('shots', TextType::class)
            ->add('requiredStrength', TextType::class)
            ->add('bulk', NumberType::class)
            ->add('recoil', NumberType::class)
            ->add('cost', NumberType::class)
            ->add('ammo', TextType::class)
            ->add('category', TextType::class)
            ->add('technologyLevel', NumberType::class)
            ->add('world', EntityType::class, array(
              'class' => 'AppBundle:World',
              'choice_label' => 'name',
              'multiple'=>true,
              'expanded'=>true))
            ->add('imageFile', VichImageType::class, ['required' => false,'allow_delete' => true])
            ->add('save', SubmitType::class)
        ;
    }
}

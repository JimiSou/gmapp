<?php
namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Vich\UploaderBundle\Form\Type\VichImageType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;


class WearableType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class)
            ->add('damageResist', NumberType::class)
            ->add('energyResist', NumberType::class)
            ->add('radiationResist', NumberType::class)
            ->add('effect', TextType::class)
            ->add('cost', NumberType::class)
            ->add('weight', NumberType::class)
            ->add('type', TextType::class)
            ->add('slots', CollectionType::class,array(
                  'entry_type'   => ChoiceType::class,
                  'entry_options'  => array(
                      'choices'  => array(
                          'Head' => 'head',
                          'Face' => 'face',
                          'Eyewear' => 'eyes',
                          'Chest'     => 'chest',
                          'Arm Left'    => 'armL',
                          'Arm Right'    => 'armR',
                          'Leg Left' => 'legL',
                          'Leg Right' => 'legR',
                      ),
                  ),
                  'allow_delete' => true,
                  'allow_add' => true,
                  'prototype' => true,
              ))
            ->add('imageFile', VichImageType::class, ['required' => false,'allow_delete' => true])
            ->add('world', EntityType::class, array(
              'class' => 'AppBundle:World',
              'choice_label' => 'name',
              'multiple'=>true,
              'expanded'=>true))
            ->add('save', SubmitType::class);
    }
}

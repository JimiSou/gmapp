<?php
namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Vich\UploaderBundle\Form\Type\VichImageType;

class LocationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class)
            ->add('description', TextareaType::class)
            ->add('type', TextType::class)
            ->add('coordinateX', NumberType::class)
            ->add('coordinateY', NumberType::class)
            ->add('world', EntityType::class, array(
              'class' => 'AppBundle:World',
              'choice_label' => 'name'))
            ->add('imageFile', VichImageType::class, ['required' => false,'allow_delete' => true])
            ->add('world', EntityType::class, array(
              'class' => 'AppBundle:World',
              'choice_label' => 'name',
              'multiple'=>true,
              'expanded'=>true))
            ->add('save', SubmitType::class)
        ;
    }
}

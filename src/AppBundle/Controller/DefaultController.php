<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;

use AppBundle\Entity\Character;
use AppBundle\Entity\Skill;
use AppBundle\Entity\Quirk;
use AppBundle\Entity\Firearm;
use AppBundle\Entity\Item;
use AppBundle\Entity\Wearable;
use AppBundle\Entity\ResourceTable;
use AppBundle\Entity\Meelee;
use AppBundle\Entity\World;
use AppBundle\Entity\Location;
use AppBundle\Entity\Faction;
use AppBundle\Entity\Log;
use AppBundle\Form\Type\CharacterType;
use AppBundle\Form\Type\SkillType;
use AppBundle\Form\Type\QuirkType;
use AppBundle\Form\Type\FirearmType;
use AppBundle\Form\Type\ItemType;
use AppBundle\Form\Type\WearableType;
use AppBundle\Form\Type\MeeleeType;
use AppBundle\Form\Type\WorldType;
use AppBundle\Form\Type\LocationType;
use AppBundle\Form\Type\FactionType;
use AppBundle\Form\Type\LogType;
use AppBundle\Entity\Repository\CharacterRepository;
use AppBundle\Entity\Repository\SkillRepository;
use AppBundle\Entity\Repository\QuirkRepository;
use AppBundle\Entity\Repository\FirearmRepository;
use AppBundle\Entity\Repository\ItemRepository;
use AppBundle\Entity\Repository\WearableRepository;
use AppBundle\Entity\Repository\MeeleeRepository;
use AppBundle\Entity\Repository\WorldRepository;
use AppBundle\Entity\Repository\LocationRepository;
use AppBundle\Entity\Repository\FactionRepository;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
      $worlds= $this->getDoctrine()
      ->getRepository(World::class)
      ->findAll();
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
            'worlds'=> $worlds,
        ]);
      }
    /**
     * @Route("/{world}/character/{id}",defaults={"id" = 0,"world"=1}, name="character")
     */
    public function characterAction(Request $request,$id,$world)
    {

      $character = new Character();
      if($id>0)
      {
        $character= $this->getDoctrine()
        ->getRepository(Character::class)
        ->find($id);
      }

      $form = $this->createForm(CharacterType::class, $character);

      $form->handleRequest($request);

      if ($form->isSubmitted() && $form->isValid())
      {
        $formData = $form->getData();
        $em = $this->getDoctrine()->getManager();
        if($id==0)
        {
          $em->persist($formData);
        }
        $em->flush();
      }
      $items=$this->getDoctrine()
      ->getRepository(Item::class)
      ->findAll();
      $firearms=$this->getDoctrine()
      ->getRepository(Firearm::class)
      ->findAll();
      $wearables=$this->getDoctrine()
      ->getRepository(Wearable::class)
      ->findAll();
      $meelees=$this->getDoctrine()
      ->getRepository(Meelee::class)
      ->findAll();
      $skills=$this->getDoctrine()
      ->getRepository(Skill::class)
      ->findAll();
      $quirks=$this->getDoctrine()
      ->getRepository(Quirk::class)
      ->findAll();
      $effects=json_decode($character->getEffects());
      foreach($effects->buyable as $keya => $valuea){
      if($valuea->class=="quirk")
      {
        foreach ($quirks as $keyb => $valueb) {
          if($valueb->getId()==$valuea->id)
          {
            $effects->buyable[$keya]->description=$valueb->getDescription();
          }
        }
      }
      if($valuea->class=="skill")
      {
        foreach ($skills as $keyb => $valueb) {
          if($valueb->getId()==$valuea->id)
          {
            $effects->buyable[$keya]->description=$valueb->getDescription();
          }
        }
      }

      }
        return $this->render('default/createCharacterForm.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
            'form' => $form->createView(),
            'character'=>$character,
            'items'=>$items,
            'firearms'=>$firearms,
            'wearables'=>$wearables,
            'meelees'=>$meelees,
            'skills'=>$skills,
            'quirks'=>$quirks,
            'effects'=>$effects,
            'itemInventory'=>json_decode($character->getItemInventory()),
            'firearmInventory'=>json_decode($character->getFirearmInventory()),
            'meeleeInventory'=>json_decode($character->getMeeleeInventory()),
            'wearableInventory'=>json_decode($character->getWearableInventory()),
            'world'=>$world,
        ]);
    }

    /**
     * @Route("/api", name="api")
     */
    public function apiAction(Request $request)
    {
      if ($request->query->get('firearm'))
      {
        $result= $this->getDoctrine()
        ->getRepository(Firearm::class)
        ->findBy(array('id'=>$request->query->get('firearm')));
        if(!$result)
        {
          return new Response('Not Found');
        }
        $json=$this->getJson($result);
        return new Response($json);
      }
      if ($request->query->get('wearable'))
      {
        $result= $this->getDoctrine()
        ->getRepository(Wearable::class)
        ->findBy(array('id'=>$request->query->get('wearable')));
        if(!$result)
        {
          return new Response('Not Found');
        }
        $json=$this->getJson($result);
        return new Response($json);
      }
      if ($request->query->get('character'))
      {
        $result= $this->getDoctrine()
        ->getRepository(Character::class)
        ->findBy(array('id'=>$request->query->get('character')));
        if(!$result)
        {
          return new Response('Not Found');
        }
        $json=$this->getJson($result);
        return new Response($json);
      }
      if ($request->query->get('item'))
      {
        $result= $this->getDoctrine()
        ->getRepository(Item::class)
        ->findBy(array('id'=>$request->query->get('item')));
        if(!$result)
        {
          return new Response('Not Found');
        }

        $json=$this->getJson($result);
        return new Response($json);
      }
      if ($request->query->get('meelee'))
      {
        $result= $this->getDoctrine()
        ->getRepository(Meelee::class)
        ->findBy(array('id'=>$request->query->get('meelee')));
        if(!$result)
        {
          return new Response('Not Found');
        }
        $json=$this->getJson($result);
        return new Response($json);
      }
      return new Response('Empty');
    }

    public function getJson($array)
    {
      if(key_exists(0,$array)&&!key_exists(1,$array))
      {
        $array=$array[0];
      }
      $serializer = new Serializer(array(new GetSetMethodNormalizer()), array('json' => new JsonEncoder()));
      $json = $serializer->serialize($array, 'json');
      return $json;
    }

    /**
     * @Route("/{world}/characters",defaults={"world"=1}, name="character_all")
     */
    public function characterAllAction(Request $request,$world)
    {
      $characters= $this->getDoctrine()
      ->getRepository(Character::class)
      ->findAll();
//->getByWorld($world);

      return $this->render('default/characterAll.html.twig', [
          'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
          'characters' => $characters,
          'world'=>$world,
      ]);
    }

    /**
     * @Route("/{world}/skill/{id}",defaults={"id" = 0,"world"=1}, name="skill")
     */
    public function skillAction(Request $request,$id,$world)
    {
      $skill = new Skill();
      if($id>0)
      {
        $skill= $this->getDoctrine()
        ->getRepository(Skill::class)
        ->find($id);
        $em = $this->getDoctrine()->getManager();
        $entityName = $em->getMetadataFactory()->getMetadataFor(get_class($skill))->getName();
        $entityName = explode('\\',$entityName);
        $title='Edit '.$entityName[2].' id: '.$id;
      }
      else {
        $em = $this->getDoctrine()->getManager();
        $entityName = $em->getMetadataFactory()->getMetadataFor(get_class($skill))->getName();
        $entityName = explode('\\',$entityName);
        $title='New '.$entityName[2];
      }
      $form = $this->createForm(SkillType::class, $skill);

      $form->handleRequest($request);

      if ($form->isSubmitted() && $form->isValid())
      {
        $formData = $form->getData();
        $em = $this->getDoctrine()->getManager();

        if($id==0)
        {
          $em->persist($formData);
        }
        $em->flush();
        if($id==0)
        {
          return $this->redirect($this->generateUrl('skill', array('id' => $formData->getId())));
        }
      }


        return $this->render('default/createEntityForm.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
            'form' => $form->createView(),
            'title'=>$title,
            'world'=>$world,
        ]);
    }

    /**
     * @Route("/{world}/skills",defaults={"world"=1}, name="skill_all")
     */
    public function skillAllAction(Request $request,$world)
    {
      $skills= $this->getDoctrine()
      ->getRepository(Skill::class)
      ->findAll();
      //->getByWorld($world);

      return $this->render('default/skillAll.html.twig', [
          'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
          'skills' => $skills,
          'world'=>$world,
      ]);
    }

    /**
     * @Route("/{world}/quirk/{id}",defaults={"id" = 0,"world"=1}, name="quirk")
     */
    public function quirkAction(Request $request,$id,$world)
    {
      $quirk = new Quirk();
      if($id>0)
      {
        $quirk= $this->getDoctrine()
        ->getRepository(Quirk::class)
        ->find($id);
        $em = $this->getDoctrine()->getManager();
        $entityName = $em->getMetadataFactory()->getMetadataFor(get_class($quirk))->getName();
        $entityName = explode('\\',$entityName);
        $title='Edit '.$entityName[2].' id: '.$id;
      }
      else {
        $em = $this->getDoctrine()->getManager();
        $entityName = $em->getMetadataFactory()->getMetadataFor(get_class($quirk))->getName();
        $entityName = explode('\\',$entityName);
        $title='New '.$entityName[2];
      }
      $form = $this->createForm(QuirkType::class, $quirk);

      $form->handleRequest($request);

      if ($form->isSubmitted() && $form->isValid())
      {
        $formData = $form->getData();
        $em = $this->getDoctrine()->getManager();
        if($id==0)
        {
          $em->persist($formData);
        }
        $em->flush();


      }
        return $this->render('default/createEntityForm.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
            'form' => $form->createView(),
            'title'=>$title,
            'world'=>$world,
        ]);
    }

    /**
     * @Route("/{world}/quirks",defaults={"world"=1}, name="quirk_all")
     */
    public function quirkAllAction(Request $request,$world)
    {
      $quirks= $this->getDoctrine()
      ->getRepository(Quirk::class)
      ->findAll();
//->getByWorld($world);

      return $this->render('default/quirkAll.html.twig', [
          'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
          'quirks' => $quirks,
          'world'=>$world,
      ]);
    }

    /**
     * @Route("/{world}/firearm/{id}",defaults={"id" = 0,"world"=1}, name="firearm")
     */
    public function firearmAction(Request $request,$id,$world)
    {
      $firearm = new Firearm();
      if($id>0)
      {
        $firearm= $this->getDoctrine()
        ->getRepository(Firearm::class)
        ->find($id);
        $em = $this->getDoctrine()->getManager();
        $entityName = $em->getMetadataFactory()->getMetadataFor(get_class($firearm))->getName();
        $entityName = explode('\\',$entityName);
        $title='Edit '.$entityName[2].' id: '.$id;
      }
      else {
        $em = $this->getDoctrine()->getManager();
        $entityName = $em->getMetadataFactory()->getMetadataFor(get_class($firearm))->getName();
        $entityName = explode('\\',$entityName);
        $title='New '.$entityName[2];
      }
      $form = $this->createForm(FirearmType::class, $firearm);

      $form->handleRequest($request);

      if ($form->isSubmitted() && $form->isValid())
      {
        $formData = $form->getData();
        $em = $this->getDoctrine()->getManager();
        if($id==0)
        {
          $em->persist($formData);
        }
        $em->flush();


      }
        return $this->render('default/createEntityForm.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
            'form' => $form->createView(),
            'title'=>$title,
            'world'=>$world,
        ]);
    }

    /**
     * @Route("/{world}/firearms",defaults={"world"=1}, name="firearm_all")
     */
    public function firearmAllAction(Request $request,$world)
    {
      $firearms= $this->getDoctrine()
      ->getRepository(Firearm::class)
      ->findAll();
//->getByWorld($world);

      return $this->render('default/firearmAll.html.twig', [
          'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
          'firearms' => $firearms,
          'world'=>$world,
      ]);
    }
    /**
     * @Route("/{world}/item/{id}",defaults={"id" = 0,"world"=1}, name="item")
     */
    public function itemAction(Request $request,$id,$world)
    {
      $item = new Item();
      if($id>0)
      {
        $item= $this->getDoctrine()
        ->getRepository(Item::class)
        ->find($id);
        $em = $this->getDoctrine()->getManager();
        $entityName = $em->getMetadataFactory()->getMetadataFor(get_class($item))->getName();
        $entityName = explode('\\',$entityName);
        $title='Edit '.$entityName[2].' id: '.$id;
      }
      else {
        $em = $this->getDoctrine()->getManager();
        $entityName = $em->getMetadataFactory()->getMetadataFor(get_class($item))->getName();
        $entityName = explode('\\',$entityName);
        $title='New '.$entityName[2];
      }
      $form = $this->createForm(ItemType::class, $item);

      $form->handleRequest($request);

      if ($form->isSubmitted() && $form->isValid())
      {
        $formData = $form->getData();
        $em = $this->getDoctrine()->getManager();
        if($id==0)
        {
          $em->persist($formData);
        }
        $em->flush();


      }
        return $this->render('default/createEntityForm.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
            'form' => $form->createView(),
            'title'=>$title,
            'world'=>$world,
        ]);
    }

    /**
     * @Route("/{world}/items",defaults={"world"=1}, name="item_all")
     */
    public function itemAllAction(Request $request,$world)
    {
      $items= $this->getDoctrine()
      ->getRepository(Item::class)
      ->findAll();
//->getByWorld($world);

      return $this->render('default/itemAll.html.twig', [
          'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
          'items' => $items,
          'world'=>$world,
      ]);
    }
    /**
     * @Route("/{world}/wearable/{id}",defaults={"id" = 0,"world"=1}, name="wearable")
     */
    public function wearableAction(Request $request,$id,$world)
    {
      $wearable = new Wearable();
      if($id>0)
      {
        $wearable= $this->getDoctrine()
        ->getRepository(Wearable::class)
        ->find($id);
        $em = $this->getDoctrine()->getManager();
        $entityName = $em->getMetadataFactory()->getMetadataFor(get_class($wearable))->getName();
        $entityName = explode('\\',$entityName);
        $title='Edit '.$entityName[2].' id: '.$id;
      }
      else {
        $em = $this->getDoctrine()->getManager();
        $entityName = $em->getMetadataFactory()->getMetadataFor(get_class($wearable))->getName();
        $entityName = explode('\\',$entityName);
        $title='New '.$entityName[2];
      }
      $form = $this->createForm(WearableType::class, $wearable);

      $form->handleRequest($request);

      if ($form->isSubmitted() && $form->isValid())
      {
        $formData = $form->getData();
        $em = $this->getDoctrine()->getManager();
        if($id==0)
        {
          $em->persist($formData);
        }
        $em->flush();


      }
        return $this->render('default/createEntityForm.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
            'form' => $form->createView(),
            'title'=>$title,
            'world'=>$world,
        ]);
    }

    /**
     * @Route("/{world}/wearables",defaults={"world"=1}, name="wearable_all")
     */
    public function wearableAllAction(Request $request,$world)
    {
      $wearables= $this->getDoctrine()
      ->getRepository(Wearable::class)
      ->findAll();
//->getByWorld($world);

      return $this->render('default/wearableAll.html.twig', [
          'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
          'wearables' => $wearables,
          'world'=>$world,
      ]);
    }

    /**
     * @Route("/{world}/meelees",defaults={"world"=1}, name="meelee_all")
     */
    public function meeleeAllAction(Request $request,$world)
    {
      $meelees= $this->getDoctrine()
      ->getRepository(Meelee::class)
      ->findAll();
//->getByWorld($world);

      return $this->render('default/meeleeAll.html.twig', [
          'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
          'meelees' => $meelees,
          'world'=>$world,
      ]);
    }

    /**
     * @Route("/{world}/meelee/{id}",defaults={"id" = 0,"world"=1}, name="meelee")
     */
    public function meeleeAction(Request $request,$id,$world)
    {
      $meelee = new Meelee();
      if($id>0)
      {
        $meelee= $this->getDoctrine()
        ->getRepository(Meelee::class)
        ->find($id);
        $em = $this->getDoctrine()->getManager();
        $entityName = $em->getMetadataFactory()->getMetadataFor(get_class($meelee))->getName();
        $entityName = explode('\\',$entityName);
        $title='Edit '.$entityName[2].' id: '.$id;
      }
      else {
        $em = $this->getDoctrine()->getManager();
        $entityName = $em->getMetadataFactory()->getMetadataFor(get_class($meelee))->getName();
        $entityName = explode('\\',$entityName);
        $title='New '.$entityName[2];
      }
      $form = $this->createForm(MeeleeType::class, $meelee);

      $form->handleRequest($request);

      if ($form->isSubmitted() && $form->isValid())
      {
        $formData = $form->getData();
        $em = $this->getDoctrine()->getManager();
        if($id==0)
        {
          $em->persist($formData);
        }
        $em->flush();


      }
        return $this->render('default/createEntityForm.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
            'form' => $form->createView(),
            'title'=>$title,
            'world'=>$world,
        ]);
    }

    /**
     * @Route("/worlds", name="world_all")
     */
    public function worldAllAction(Request $request)
    {
      $worlds= $this->getDoctrine()
      ->getRepository(World::class)
      ->findAll();

      return $this->render('default/worldAll.html.twig', [
          'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
          'worlds' => $worlds,
      ]);
    }

    /**
     * @Route("/world/{id}",defaults={"id" = 0}, name="world")
     */
    public function worldAction(Request $request,$id)
    {
      $world = new World();
      if($id>0)
      {
        $world= $this->getDoctrine()
        ->getRepository(World::class)
        ->find($id);
        $em = $this->getDoctrine()->getManager();
        $entityName = $em->getMetadataFactory()->getMetadataFor(get_class($world))->getName();
        $entityName = explode('\\',$entityName);
        $title='Edit '.$entityName[2].' id: '.$id;
      }
      else {
        $em = $this->getDoctrine()->getManager();
        $entityName = $em->getMetadataFactory()->getMetadataFor(get_class($world))->getName();
        $entityName = explode('\\',$entityName);
        $title='New '.$entityName[2];
      }
      $form = $this->createForm(WorldType::class, $world);

      $form->handleRequest($request);

      if ($form->isSubmitted() && $form->isValid())
      {
        $formData = $form->getData();
        $em = $this->getDoctrine()->getManager();
        if($id==0)
        {
          $em->persist($formData);
        }
        $em->flush();


      }
        return $this->render('default/createEntityForm.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
            'form' => $form->createView(),
            'title'=>$title
        ]);
    }

    /**
     * @Route("/{world}/locations",defaults={"world"=1}, name="location_all")
     */
    public function locationAllAction(Request $request,$world)
    {
      $locations= $this->getDoctrine()
      ->getRepository(Location::class)
      ->findAll();
//->getByWorld($world);

      return $this->render('default/locationAll.html.twig', [
          'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
          'locations' => $locations,
          'world'=>$world,
      ]);
    }

    /**
     * @Route("/{world}/location/{id}",defaults={"id" = 0,"world"=1}, name="location")
     */
    public function locationAction(Request $request,$id,$world)
    {
      $location = new Location();
      if($id>0)
      {
        $location= $this->getDoctrine()
        ->getRepository(Location::class)
        ->find($id);
        $em = $this->getDoctrine()->getManager();
        $entityName = $em->getMetadataFactory()->getMetadataFor(get_class($location))->getName();
        $entityName = explode('\\',$entityName);
        $title='Edit '.$entityName[2].' id: '.$id;
      }
      else {
        $em = $this->getDoctrine()->getManager();
        $entityName = $em->getMetadataFactory()->getMetadataFor(get_class($location))->getName();
        $entityName = explode('\\',$entityName);
        $title='New '.$entityName[2];
      }
      $form = $this->createForm(LocationType::class, $location);

      $form->handleRequest($request);

      if ($form->isSubmitted() && $form->isValid())
      {
        $formData = $form->getData();
        $em = $this->getDoctrine()->getManager();
        if($id==0)
        {
          $em->persist($formData);
        }
        $em->flush();


      }
        return $this->render('default/createEntityForm.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
            'form' => $form->createView(),
            'title'=>$title,
            'world'=>$world,
        ]);
    }

    /**
     * @Route("/{world}/factions",defaults={"world"=1}, name="faction_all")
     */
    public function factionAllAction(Request $request,$world)
    {
      $factions= $this->getDoctrine()
      ->getRepository(Faction::class)
      ->findAll();
//->getByWorld($world);

      return $this->render('default/factionAll.html.twig', [
          'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
          'factions' => $factions,
          'world'=>$world,
      ]);
    }

    /**
     * @Route("/{world}/faction/{id}",defaults={"id" = 0,"world"=1}, name="faction")
     */
    public function factionAction(Request $request,$id,$world)
    {
      $faction = new Faction();
      if($id>0)
      {
        $faction= $this->getDoctrine()
        ->getRepository(Faction::class)
        ->find($id);
        $em = $this->getDoctrine()->getManager();
        $entityName = $em->getMetadataFactory()->getMetadataFor(get_class($faction))->getName();
        $entityName = explode('\\',$entityName);
        $title='Edit '.$entityName[2].' id: '.$id;
      }
      else {
        $em = $this->getDoctrine()->getManager();
        $entityName = $em->getMetadataFactory()->getMetadataFor(get_class($faction))->getName();
        $entityName = explode('\\',$entityName);
        $title='New '.$entityName[2];
      }
      $form = $this->createForm(FactionType::class, $faction);

      $form->handleRequest($request);

      if ($form->isSubmitted() && $form->isValid())
      {
        $formData = $form->getData();
        $em = $this->getDoctrine()->getManager();
        if($id==0)
        {
          $em->persist($formData);
        }
        $em->flush();


      }
        return $this->render('default/createEntityForm.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
            'form' => $form->createView(),
            'title'=>$title,
            'world'=>$world,
        ]);
    }

    /**
     * @Route("/{world}/rolls",defaults={"world"=1}, name="rolls")
     */
    public function rollsAction(Request $request,$world)
    {
      $characters= $this->getDoctrine()
      ->getRepository(Character::class)
      ->findAll();
//->getByWorld($world);
      $skills= $this->getDoctrine()
      ->getRepository(Skill::class)
      ->findAll();
//->getByWorld($world);
      $reactionTable= $this->getDoctrine()
      ->getRepository(ResourceTable::class)
      ->findBy(array('name'=>'Reactions Level'));

      $log = new Log();
      $form = $this->createForm(LogType::class, $log);
      $form->handleRequest($request);

      return $this->render('default/rolls.html.twig', [
          'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
          'characters'=> $characters,
          'skills'=>$skills,
          'reactionTable'=>json_encode($reactionTable),
          'form'=> $form->createView(),
          'world'=>$world,
      ]);
    }

    /**
     * @Route("/upload", name="upload")
     */
    public function uploadAction(Request $request)
    {
      $target_dir = $request->query->get("dir");
      if($target_dir==null)
      {
        $target_dir = "uploads/";
      }

      $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
      $uploadOk = 1;
      $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);

      if(isset($_POST["submit"])) {

      }
      // Check if file already exists
      if (file_exists($target_file)) {
        echo "Sorry, file already exists.";
        $uploadOk = 0;
      }
      // Check file size
      if ($_FILES["fileToUpload"]["size"] > 5000000) {
        echo "Sorry, your file is too large.";
        $uploadOk = 0;
      }

      if ($uploadOk == 0) {
        echo "Sorry, your file was not uploaded.";
      // if everything is ok, try to upload file
      } else {
        if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
            echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
        } else {
            echo "Sorry, there was an error uploading your file.";
        }
      }
      die;
    }

    /**
     * @Route("/{world}/{entity}/import",defaults={"world"=1}, name="csv_import")
     */
    public function csvImportAction(Request $request,$entity,$world)
    {
      $serializer = new Serializer([new ObjectNormalizer()], [new CsvEncoder()]);

      $data = $serializer->decode(file_get_contents(__DIR__.'/../../../web/uploads/files/import.csv'), 'csv');
      $em=$this->getDoctrine()->getManager();

      switch ($entity){
        case 'skills':

          foreach($data as $row) {
            $object = new Skill;
            $object->setName($row['name']);
            $object->setDifficulty($row['difficulty']);
            $object->setAttribute($row['attribute']);
            $object->setDefaultModifier($row['defaultModifier']);
            $object->setDescription($row['description']);
            $object->setCost(0);
            $object->setWorld($world);

            $em->persist($object);
          }
          $em->flush();
          $em->clear();
          break;
       case 'quirks':

        foreach($data as $row) {
          $object = new Quirk;
          $object->setName($row['name']);
          $object->setCost($row['cost']);
          $object->setMaxLevel($row['maxLevel']);
          $object->setDescription($row['description']);
          $object->setNote($row['note']);
          $object->setType($row['type']);
          $object->setCategory($row['category']);
          $object->setWorld($world);

          $em->persist($object);
        }
        $em->flush();
        $em->clear();
      break;
      case 'firearms':

        foreach($data as $row) {
          $object = new Firearm;
          $object->setName($row['name']);
          $object->setDamage($row['damage']);
          $object->setDamageType($row['damageType']);
          $object->setAccuracy($row['accuracy']);
          $object->setRangeMin($row['rangeMin']);
          $object->setRangeMax($row['rangeMax']);
          $object->setWeight($row['weight']);
          $object->setRateOfFire($row['rateOfFire']);
          $object->setShots($row['shots']);
          $object->setRequiredStrength($row['requiredStrength']);
          $object->setBulk($row['bulk']);
          $object->setRecoil($row['recoil']);
          $object->setCost($row['cost']);
          $object->setAmmo($row['ammo']);
          $object->setCategory($row['category']);
          $object->setTechnologyLevel($row['technologyLevel']);
          $object->setWorld($world);

          $em->persist($object);
        }
        $em->flush();
        $em->clear();
        break;
      case 'items':

       foreach($data as $row) {
         $object = new Item;
         $object->setName($row['name']);
         $object->setCost($row['cost']);
         $object->setWeight($row['weight']);
         $object->setDescription($row['description']);
         $object->setType($row['type']);
         $object->setWorld($world);

         $em->persist($object);
       }
       $em->flush();
       $em->clear();
     break;
     case 'wearables':

        foreach($data as $row) {
          $object = new Wearable;
          $object->setName($row['name']);
          $object->setDamageResist($row['damageResist']);
          $object->setEnergyResist($row['energyResist']);
          $object->setRadiationResist($row['radiationResist']);
          $object->setCost($row['cost']);
          $object->setWeight($row['weight']);
          $object->setEffect($row['effect']);
          $object->setType($row['type']);
          $slots=$row['slots'];
          $slotsArray=json_encode(explode(',',$slots));
          $object->setSlots($slotsArray);
          $object->setWorld($world);

          $em->persist($object);
        }
        $em->flush();
        $em->clear();
      break;
      case 'meelees':

         foreach($data as $row) {
           $object = new Meelee;
           $object->setName($row['name']);
           $object->setThrustDamage($row['thrustDamage']);
           $object->setThrustReach($row['thrustReach']);
           $object->setSwingDamage($row['swingDamage']);
           $object->setSwingReach($row['swingReach']);
           $object->setCost($row['cost']);
           $object->setWeight($row['weight']);
           $object->setNotes($row['notes']);
           $object->setStrength($row['strength']);
           $object->setParry($row['parry']);
           $object->setTechnologyLevel($row['technologyLevel']);
           $object->setCategory($row['category']);
           $object->setWorld($world);

           $em->persist($object);
         }
         $em->flush();
         $em->clear();
       break;
      }
      return $this->redirect('/'.$entity);
    }

    /**
     * @Route("/map", name="worldMap")
     */
    public function worldMapAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/worldMap.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
        ]);
    }
    /**
     * @Route("/commander-ui", name="commander_ui")
     */
    public function commanderUiAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/commander.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
        ]);
    }

    /**
     * @Route("/commander", name="commander")
     */
    public function commanderAction(Request $request)
    {
        if($request->request->get('cmd') !== null)
        {
          $command = $request->request->get('cmd');
          $command = explode(" ",$command);
          if(count($command)>1)
          {
            switch ($command[0]) {
              case 'set':
                  //set class id var=value,var2=value2,...
                  if(array_key_exists(1, $command)&&array_key_exists(2, $command)&&array_key_exists(3, $command))
                  {
                        $result=$this->getDoctrine()
                        ->getRepository('AppBundle:'.$command[1])
                        ->find($command[2]);
                        if($result!=null)
                        {
                          $vars=explode(",", $command[3]);
                          $em=$this->getDoctrine()->getManager();
                          foreach ($vars as $key => $value) {
                            $varVal=explode("=", $value);
                            if (array_key_exists(1, $varVal)) {
                              $name='set'.ucfirst($varVal[0]);
                              $value=$varVal[1];
                              $result->$name($value);
                            }
                          }
                          $em->persist($result);
                          $em->flush();
                          return new Response('Commander says: Done!!');
                        }
                        else {
                          return new Response('Commander says: '.$command[1].' ID '.$command[2].' cannot be found.');
                        }

                  }
                  else {
                    return new Response('Commander understands "'.$command[0].'" rest is incomplete or missing. -500-');
                  }

            break;
            case 'give':
                $serializer = new Serializer(array(new GetSetMethodNormalizer()), array('json' => new JsonEncoder()));
                //give class id to id count
                if(array_key_exists(1, $command)&&array_key_exists(2, $command)&&array_key_exists(3, $command)&&array_key_exists(4, $command)&&array_key_exists(5, $command)&&$command[3]=="to")
                {
                      $result=$this->getDoctrine()
                      ->getRepository('AppBundle:'.ucfirst($command[1]))
                      ->find($command[2]);
                      $character=$this->getDoctrine()
                      ->getRepository(Character::class)
                      ->find($command[4]);
                      if($result!=null&&$character!=null)
                      {
                        $add=false;
                        $em=$this->getDoctrine()->getManager();
                        $name='get'.ucfirst($command[1]).'Inventory';
                        $inventory=$character->$name();
                        $inventory=json_decode($inventory,true);

                        foreach ($inventory as $key => $value) {
                          if($command[2]==$value["id"])
                          {
                            $inventory[$key]["count"]=$value["count"]+$command[5];
                            $add=true;
                            break;
                          }
                        }
                        if(!$add)
                        {
                          $result = json_decode($serializer->serialize($result, 'json'),true);
                          $result["count"]=$command[5];
                          $inventory[]=$result;
                        }
                        $inventory=json_encode($inventory);
                        $name='set'.ucfirst($command[1]).'Inventory';
                        $character->$name($inventory);

                        $em->persist($character);
                        $em->flush();
                        return new Response('Commander says: Done!!');
                      }
                      else {
                        return new Response('Commander says: '.$command[1].' ID '.$command[2].' cannot be found.');
                      }

                }
                else {
                  return new Response('Commander understands "'.$command[0].'" rest is incomplete or missing. -500-');
                }

              break;
              case 'remove':
                $serializer = new Serializer(array(new GetSetMethodNormalizer()), array('json' => new JsonEncoder()));
                //remove class id from id count
                if(array_key_exists(1, $command)&&array_key_exists(2, $command)&&array_key_exists(3, $command)&&array_key_exists(4, $command)&&array_key_exists(5, $command)&&$command[3]=="from")
                {
                  $character=$this->getDoctrine()
                  ->getRepository(Character::class)
                  ->find($command[4]);
                  if($character!=null)
                  {
                    $em=$this->getDoctrine()->getManager();
                    $name='get'.ucfirst($command[1]).'Inventory';
                    $inventory=$character->$name();
                    $inventory=json_decode($inventory,true);

                    foreach ($inventory as $key => $value) {
                      if($command[2]==$value["id"])
                      {
                        if($value["count"]-$command[5] <= 0)
                        {
                          unset($inventory[$key]);
                          break;
                        }
                        else {
                          $inventory[$key]["count"]=$value["count"]-$command[5];
                          break;
                        }

                      }
                    }
                    $inventory=json_encode($inventory);
                    $name='set'.ucfirst($command[1]).'Inventory';
                    $character->$name($inventory);
                    $em->persist($character);
                    $em->flush();
                    return new Response('Commander says: Done!!');
                  }
                }
              break;

              default:
                  return new Response('Commander doesn`t understand -404-');
                break;
            }
          }
        }
    }

    /**
     * @Route("/create-log", name="log_create")
     */
    public function createLogAction(Request $request)
    {
      if($request->request->get("data"))
      {
        $data=json_decode($request->request->get("data"),true);
        $em=$this->getDoctrine()->getManager();
        $log = new Log;
        foreach ($data as $key => $value) {
          $name='set'.ucfirst($key);
          if($key!='character'&&$key!='otherCharacter'&&$key!='location'&&$key!='skill'&&$key!='faction'&&$key!='world'&&$key!='date'){
            $log->$name($value);
          } else{
            if($key=='date')
            {
              $item= \DateTime::createFromFormat('Y-m-d H:i:s', $value);
              $item->format("Y-m-d H:i:s");
            }
            else {
              if($key=='otherCharacter')
              {
                $key='character';
              }
              $item=$this->getDoctrine()
              ->getRepository('AppBundle:'.ucfirst($key))
              ->find($value);
            }

            $log->$name($item);
          }
        }
        $em->persist($log);
        $em->flush();
        return new JsonResponse(['result'=>'Success']);
      }
      else {
        return new JsonResponse(['result'=>'Error - No data recieved.']);
      }
    }

    protected function getBaseRepository()
    {
        return new BaseRepository();
    }
}

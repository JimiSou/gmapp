<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;

use AppBundle\Entity\Character;
use AppBundle\Entity\Skill;
use AppBundle\Entity\Advantage;
use AppBundle\Entity\Disadvantage;
use AppBundle\Entity\Firearm;
use AppBundle\Entity\Item;
use AppBundle\Entity\Wearable;
use AppBundle\Form\Type\CharacterType;
use AppBundle\Form\Type\SkillType;
use AppBundle\Form\Type\AdvantageType;
use AppBundle\Form\Type\DisadvantageType;
use AppBundle\Form\Type\FirearmType;
use AppBundle\Form\Type\ItemType;
use AppBundle\Form\Type\WearableType;

class RaaaceController extends Controller
{
    /**
     * @Route("/raaace/car", name="car_test")
     */
    public function indexAction(Request $request)
    {
        /*$filename='./uploads/auto1.png';
        $img = $this->getImage($filename);
        $removeColour = imagecolorallocate($img, 255, 0, 255);
        imagecolortransparent($img, $removeColour);
        imagesavealpha($img, true);
        $transColor = imagecolorallocatealpha($img, 255, 0, 255, 175);
        imagefill($img,0, 0, $transColor);
        imagepng($img,'./uploads/auto.png');*/
        $this->changeColourInImage('./uploads/carsmall.jpg','./uploads/output.png',array(255,255,255),array(251, 40, 255),50);
        return new Response ('OK');
    }
    /**
     * @Route("/raaace", name="game")
     */
    public function gameAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('raaace/game.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,

        ]);
    }

    function changeColourInImage($path,$finalPath,$lookfor,$changeto,$margin) {
      $src = $this->resize_image('./uploads/auto.jpg',300, 225);
        $src = imagecreatefromJPEG($path) or die('Problem with source');
        //bottom -37px
        $imageY=imagesy($src);
        $imageX=imagesx($src);
        $out = ImageCreateTrueColor($imageX,$imageY) or die('Problem In Creating image');
        //imagecopyresampled($out, $src, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);

        $topY=[];
        $leftCut=[];
        $rightCut=[];
        //top to bottom
        for ($x = 0; $x < $imageX; $x++) {
          $donothing=false;
          $pixelCounter=0;
            for ($y = 0; $y < $imageY; $y++) {
                //echo 'x: '.$x.' y: '.$y.'<br>';
                $src_pix = imagecolorat($src,$x,$y);
                $src_pix_array = $this->rgb_to_array($src_pix);
                if($donothing==false)
                {
                  if (($src_pix_array[0] <= $lookfor[0]&&$src_pix_array[0] >= $lookfor[0]-$margin) && ($src_pix_array[1] <= $lookfor[1]&&$src_pix_array[1] >= $lookfor[1]-$margin) && ($src_pix_array[2] <= $lookfor[2]&&$src_pix_array[2] >= $lookfor[2]-$margin)) {
                      $src_pix_array[0] = $changeto[0];
                      $src_pix_array[1] = $changeto[1];
                      $src_pix_array[2] = $changeto[2];

                  }
                  else {
                    if( $x>$imageX/20 && $x<$imageX-$imageX/20 )
                    {
                      if($imageY-$y>40)
                      {
                        $topY[]=$imageY-$y;
                      }
                    }
                    $donothing=true;
                  }
                }
                imagesetpixel($out, $x, $y, imagecolorallocate($out, $src_pix_array[0], $src_pix_array[1], $src_pix_array[2]));
            }
        }

        //right to left
        $src = $out;//imagecreatefrompng($finalPath) or die('Problem with source');
        for ($y = 0; $y < $imageY; $y++) {
          $donothing=false;
        for ($x = $imageX-1; $x >= 0; $x--) {


                //echo 'x: '.$x.' y: '.$y.'<br>';
                $src_pix = imagecolorat($src,$x,$y);
                $src_pix_array = $this->rgb_to_array($src_pix);
                if($donothing==false)
                {
                  if (($src_pix_array[0] <= $lookfor[0]&&$src_pix_array[0] >= $lookfor[0]-$margin) && ($src_pix_array[1] <= $lookfor[1]&&$src_pix_array[1] >= $lookfor[1]-$margin) && ($src_pix_array[2] <= $lookfor[2]&&$src_pix_array[2] >= $lookfor[2]-$margin)) {
                      $src_pix_array[0] = $changeto[0];
                      $src_pix_array[1] = $changeto[1];
                      $src_pix_array[2] = $changeto[2];
                  }
                  else {
                    if(($src_pix_array[0] != $changeto[0]) && ($src_pix_array[1] != $changeto[1]) && ($src_pix_array[2] != $changeto[2]))
                    {
                      $donothing=true;
                    }

                  }
                }
                imagesetpixel($out, $x, $y, imagecolorallocate($out, $src_pix_array[0], $src_pix_array[1], $src_pix_array[2]));
            }
        }

        //left to right
        $src = $out;
        for ($y = 0; $y < $imageY; $y++) {
        $donothing=false;
        for ($x = 0; $x < $imageX; $x++) {


                //echo 'x: '.$x.' y: '.$y.'<br>';
                $src_pix = imagecolorat($src,$x,$y);
                $src_pix_array = $this->rgb_to_array($src_pix);
                if($donothing==false)
                {
                  if (($src_pix_array[0] <= $lookfor[0]&&$src_pix_array[0] >= $lookfor[0]-$margin) && ($src_pix_array[1] <= $lookfor[1]&&$src_pix_array[1] >= $lookfor[1]-$margin) && ($src_pix_array[2] <= $lookfor[2]&&$src_pix_array[2] >= $lookfor[2]-$margin)) {
                      $src_pix_array[0] = $changeto[0];
                      $src_pix_array[1] = $changeto[1];
                      $src_pix_array[2] = $changeto[2];
                  }
                  else {
                    if(($src_pix_array[0] != $changeto[0]) && ($src_pix_array[1] != $changeto[1]) && ($src_pix_array[2] != $changeto[2]))
                    {
                      $donothing=true;
                    }

                  }
                }
                imagesetpixel($out, $x, $y, imagecolorallocate($out, $src_pix_array[0], $src_pix_array[1], $src_pix_array[2]));
            }
        }

        //bottom to top WHITE
        $src = $out;
        for ($x = 0; $x < $imageX; $x++) {
        $donothing=false;
        for ($y = $imageY-1; $y >=0 ; $y--) {


                //echo 'x: '.$x.' y: '.$y.'<br>';
                $src_pix = imagecolorat($src,$x,$y);
                $src_pix_array = $this->rgb_to_array($src_pix);
                if($donothing==false)
                {
                  if (($src_pix_array[0] <= $lookfor[0]&&$src_pix_array[0] >= $lookfor[0]-$margin) && ($src_pix_array[1] <= $lookfor[1]&&$src_pix_array[1] >= $lookfor[1]-$margin) && ($src_pix_array[2] <= $lookfor[2]&&$src_pix_array[2] >= $lookfor[2]-$margin)) {
                      $src_pix_array[0] = $changeto[0];
                      $src_pix_array[1] = $changeto[1];
                      $src_pix_array[2] = $changeto[2];
                  }
                  else {
                    if(($src_pix_array[0] != $changeto[0]) && ($src_pix_array[1] != $changeto[1]) && ($src_pix_array[2] != $changeto[2]))
                    {
                      $donothing=true;
                    }

                  }
                }
                imagesetpixel($out, $x, $y, imagecolorallocate($out, $src_pix_array[0], $src_pix_array[1], $src_pix_array[2]));
            }
        }
        //bottom to top GREY
        $src = $out;
        $lookfor=array(136,135,131);
        $margin=30;
        for ($x = 0; $x < $imageX; $x++) {
        $donothing=false;
        for ($y = $imageY-1; $y >= 0 ; $y--) {


                //echo 'x: '.$x.' y: '.$y.'<br>';
                $src_pix = imagecolorat($src,$x,$y);
                $src_pix_array = $this->rgb_to_array($src_pix);
                if($donothing==false)
                {
                    if (($src_pix_array[0] >= $lookfor[0]-$margin&&$src_pix_array[0] <= 255) && ($src_pix_array[1] >= $lookfor[1]-$margin&&$src_pix_array[1] <= 255) && ($src_pix_array[2] >= $lookfor[2]-$margin&&$src_pix_array[2] <= 255)) {
                        $src_pix_array[0] = $changeto[0];
                        $src_pix_array[1] = $changeto[1];
                        $src_pix_array[2] = $changeto[2];
                    }
                    else {
                      if(($src_pix_array[0] != $changeto[0]) && ($src_pix_array[1] != $changeto[1]) && ($src_pix_array[2] != $changeto[2]))
                      {
                        if( $x>$imageX-$imageX/30 && $x<$imageX )
                        {
                          $bottomY[]=$y;
                        }
                        $donothing=true;
                      }

                    }
                }
                imagesetpixel($out, $x, $y, imagecolorallocate($out, $src_pix_array[0], $src_pix_array[1], $src_pix_array[2]));
            }
        }
        //bottom to top BLACK 1/3 right side only
        $src = $out;
        $margin=50;
        $bottomY=[];
        for ($x = $imageX/2; $x < $imageX; $x++) {
        $donothing=false;
        for ($y = $imageY-1; $y >= $imageY/2 ; $y--) {
              $src_pix = imagecolorat($src,$x,$y);
              $src_pix_array = $this->rgb_to_array($src_pix);
              if ($src_pix_array[0] < $margin && $src_pix_array[1] < $margin && $src_pix_array[2] <  $margin)
              {
                if($imageY-$y>40&&$imageY-$y<70)
                {
                  $bottomY[]=$imageY-$y;
                }
              }
        }}
        //print_r($topY);
        sort($topY);
        //$topY = array_reverse($topY);
        $topY=$topY[0];
        //print_r($topY);
        sort($bottomY);
        //print_r($bottomY);

        $bottomY=$bottomY[0];

        $newwidth=$imageX;
        $newheight=$imageY-$topY-$bottomY;

        $dst = imagecreatetruecolor($newwidth, $newheight);
        $color = imagecolorallocate($dst, $changeto[0], $changeto[1], $changeto[2]);
        imagecolortransparent($dst, $color);
        imagecopyresampled($dst, $out, 0, 0, 0, $topY, $newwidth, $newheight, $imageX, $newheight);
        imagepng($dst, $finalPath) or die('Problem saving output image');
        imagedestroy($out);
      }
      function rgb_to_array($rgb) {
        $a[0] = ($rgb >> 16) & 0xFF;
        $a[1] = ($rgb >> 8) & 0xFF;
        $a[2] = $rgb & 0xFF;

        return $a;
      }

      function resize_image($file, $w, $h) {
        list($width, $height) = getimagesize($file);
        $r = $width / $height;

            if ($w/$h > $r) {
                $newwidth = $h*$r;
                $newheight = $h;
            } else {
                $newheight = $w/$r;
                $newwidth = $w;
            }

        $src = imagecreatefromjpeg($file);
        $dst = imagecreatetruecolor($newwidth, $newheight);
        imagecopyresampled($dst, $src, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
        imagejpeg($dst, './uploads/carsmall.jpg');
        return $dst;
      }


}

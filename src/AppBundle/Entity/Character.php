<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\CharacterRepository")
 * @ORM\Table(name="characterr")
 * @Vich\Uploadable
 */
class Character
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     */
    private $name= 'Character Name';

    /**
     * @ORM\Column(type="text")
     */
    private $player= 'Player name';
    /**
     * @ORM\Column(type="text")
     */
    private $alias= 'Character nickname(s)';
    /**
     * @ORM\Column(type="text")
     */
    private $type= 'Character';

    /**
     * @ORM\Column(type="integer")
     */
    private $age=0;

    /**
     * @ORM\Column(type="text")
     */
    private $race= 'Race';

    /**
     * @ORM\Column(type="text")
     */
    private $appearanceDesc= 'description';
    /**
     * @ORM\Column(type="integer")
     */
    private $appearance=3;
    /**
     * @ORM\Column(type="integer")
     */
    private $reactionModifier=0;
    /**
     * @ORM\Column(type="float")
     */
    private $height=2;
    /**
     * @ORM\Column(type="integer")
     */
    private $sizeModifier=0;
    /**
     * @ORM\Column(type="integer")
     */
    private $strength=10;
    /**
     * @ORM\Column(type="integer")
     */
    private $strengthModifier=0;
    /**
     * @ORM\Column(type="integer")
     */
    private $dexterity=10;
    /**
     * @ORM\Column(type="integer")
     */
    private $dexterityModifier=0;
    /**
     * @ORM\Column(type="integer")
     */
    private $intelligence=10;
    /**
     * @ORM\Column(type="integer")
     */
    private $intelligenceModifier=0;
    /**
     * @ORM\Column(type="integer")
     */
    private $health=10;
    /**
     * @ORM\Column(type="integer")
     */
    private $healthModifier=0;
    /**
     * @ORM\Column(type="integer")
     */
    private $hitPoints=0;
    /**
     * @ORM\Column(type="integer")
     */
    private $hitPointsModifier=0;
    /**
     * @ORM\Column(type="integer")
     */
    private $hitPointsCurrent=0;
    /**
     * @ORM\Column(type="integer")
     */
    private $will=0;
    /**
     * @ORM\Column(type="integer")
     */
    private $willModifier=0;
    /**
     * @ORM\Column(type="integer")
     */
    private $perception=0;
    /**
     * @ORM\Column(type="integer")
     */
    private $perceptionModifier=0;
    /**
     * @ORM\Column(type="integer")
     */
    private $fatiguePoints=0;
    /**
     * @ORM\Column(type="integer")
     */
    private $fatiguePointsModifier=0;
    /**
     * @ORM\Column(type="integer")
     */
    private $fatiguePointsCurrent=0;
    /**
     * @ORM\Column(type="integer")
     */
    private $basicLift=0;
    /**
     * @ORM\Column(type="float")
     */
    private $basicSpeed=0;
    /**
     * @ORM\Column(type="integer")
     */
    private $basicMove=0;
    /**
     * @ORM\Column(type="float")
     */
    private $hiking=0;
    /**
     * @ORM\Column(type="integer")
     */
    private $gainedCP=0;
    /**
     * @ORM\Column(type="integer")
     */
    private $startingCP=0;
    /**
     * @ORM\Column(type="integer")
     */
    private $unusedCP=0;
    /**
     * @ORM\Column(type="integer")
     */
    private $money=0;
    /**
     * @ORM\Column(type="text")
     */
    private $damageThrust='none';
    /**
     * @ORM\Column(type="text")
     */
    private $damageSwing='none';
    /**
     * @ORM\Column(type="integer")
     */
    private $damageResistanceHead=0;
    /**
     * @ORM\Column(type="integer")
     */
    private $damageResistanceBody=0;
    /**
     * @ORM\Column(type="integer")
     */
    private $damageResistanceLeftArm=0;
    /**
     * @ORM\Column(type="integer")
     */
    private $damageResistanceRightArm=0;
    /**
     * @ORM\Column(type="integer")
     */
    private $damageResistanceLeftLeg=0;
    /**
     * @ORM\Column(type="integer")
     */
    private $damageResistanceRightLeg=0;
    /**
     * @ORM\Column(type="integer")
     */
    private $damageResistanceEyes=0;
    /**
     * @ORM\Column(type="integer")
     */
    private $damageResistanceFace=0;
    /**
     * @ORM\Column(type="integer")
     */
    private $energyResistanceHead=0;
    /**
     * @ORM\Column(type="integer")
     */
    private $energyResistanceBody=0;
    /**
     * @ORM\Column(type="integer")
     */
    private $energyResistanceLeftArm=0;
    /**
     * @ORM\Column(type="integer")
     */
    private $energyResistanceRightArm=0;
    /**
     * @ORM\Column(type="integer")
     */
    private $energyResistanceLeftLeg=0;
    /**
     * @ORM\Column(type="integer")
     */
    private $energyResistanceRightLeg=0;
    /**
     * @ORM\Column(type="integer")
     */
    private $energyResistanceEyes=0;
    /**
     * @ORM\Column(type="integer")
     */
    private $energyResistanceFace=0;
    /**
     * @ORM\Column(type="integer")
     */
    private $radiationResistanceHead=0;
    /**
     * @ORM\Column(type="integer")
     */
    private $radiationResistanceBody=0;
    /**
     * @ORM\Column(type="integer")
     */
    private $radiationResistanceLeftArm=0;
    /**
     * @ORM\Column(type="integer")
     */
    private $radiationResistanceRightArm=0;
    /**
     * @ORM\Column(type="integer")
     */
    private $radiationResistanceLeftLeg=0;
    /**
     * @ORM\Column(type="integer")
     */
    private $radiationResistanceRightLeg=0;
    /**
     * @ORM\Column(type="integer")
     */
    private $radiationResistanceEyes=0;
    /**
     * @ORM\Column(type="integer")
     */
    private $radiationResistanceFace=0;
    /**
     * @ORM\Column(type="integer")
     */
    private $parry=0;
    /**
     * @ORM\Column(type="integer")
     */
    private $block=0;
    /**
     * @ORM\Column(type="integer")
     */
    private $dodge=0;
    /**
     * @ORM\Column(type="json_array",nullable=true)
     */
    private $itemInventory="{}";
    /**
     * @ORM\Column(type="json_array",nullable=true)
     */
    private $firearmInventory="{}";
    /**
     * @ORM\Column(type="json_array",nullable=true)
     */
    private $wearableInventory="{}";
    /**
     * @ORM\Column(type="json_array",nullable=true)
     */
    private $meeleeInventory="{}";
    /**
     * @ORM\Column(type="json_array",nullable=true)
     */
    private $equip;
    /**
     * @ORM\Column(type="json_array",nullable=true)
     */
    private $naturalAttacks;
    /**
     * @ORM\Column(type="json_array",nullable=true)
     */
    private $effects="{'buyable':[],'game':[]}";
    /**
     * @ORM\Column(type="integer")
     */
    private $encumbranceLevel=0;
    /**
     *
     *
     * @Vich\UploadableField(mapping="character_image", fileNameProperty="imageName", size="imageSize")
     *
     * @var File
     */
    private $imageFile;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @var string
     */
    private $imageName;
    /**
     * @ORM\Column(type="integer")
     *
     * @var integer
     */
    private $imageSize=0;

    /**
     * @ORM\Column(type="datetime")
     *
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * @ORM\ManyToMany(targetEntity="World")
     * @ORM\JoinTable(name="character_worlds_join",
     *      joinColumns={@ORM\JoinColumn(name="character_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="world_id", referencedColumnName="id")}
     *      )
     */
    private $world;

    /**
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     *
     * @return Product
     */
    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;

        if ($image) {
          $this->updatedAt = new \DateTimeImmutable();
        }

        return $this;
    }

    /**
     * @return File|null
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    /**
     * @param string $imageName
     *
     * @return Product
     */
    public function setImageName($imageName)
    {
        $this->imageName = $imageName;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getImageName()
    {
        return $this->imageName;
    }

    /**
     * @param integer $imageSize
     *
     * @return Product
     */
    public function setImageSize($imageSize)
    {
        $this->imageSize = $imageSize;

        return $this;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->world = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Character
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set player
     *
     * @param string $player
     *
     * @return Character
     */
    public function setPlayer($player)
    {
        $this->player = $player;

        return $this;
    }

    /**
     * Get player
     *
     * @return string
     */
    public function getPlayer()
    {
        return $this->player;
    }

    /**
     * Set alias
     *
     * @param string $alias
     *
     * @return Character
     */
    public function setAlias($alias)
    {
        $this->alias = $alias;

        return $this;
    }

    /**
     * Get alias
     *
     * @return string
     */
    public function getAlias()
    {
        return $this->alias;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Character
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set age
     *
     * @param integer $age
     *
     * @return Character
     */
    public function setAge($age)
    {
        $this->age = $age;

        return $this;
    }

    /**
     * Get age
     *
     * @return integer
     */
    public function getAge()
    {
        return $this->age;
    }

    /**
     * Set race
     *
     * @param string $race
     *
     * @return Character
     */
    public function setRace($race)
    {
        $this->race = $race;

        return $this;
    }

    /**
     * Get race
     *
     * @return string
     */
    public function getRace()
    {
        return $this->race;
    }

    /**
     * Set appearanceDesc
     *
     * @param string $appearanceDesc
     *
     * @return Character
     */
    public function setAppearanceDesc($appearanceDesc)
    {
        $this->appearanceDesc = $appearanceDesc;

        return $this;
    }

    /**
     * Get appearanceDesc
     *
     * @return string
     */
    public function getAppearanceDesc()
    {
        return $this->appearanceDesc;
    }

    /**
     * Set appearance
     *
     * @param integer $appearance
     *
     * @return Character
     */
    public function setAppearance($appearance)
    {
        $this->appearance = $appearance;

        return $this;
    }

    /**
     * Get appearance
     *
     * @return integer
     */
    public function getAppearance()
    {
        return $this->appearance;
    }

    /**
     * Set reactionModifier
     *
     * @param integer $reactionModifier
     *
     * @return Character
     */
    public function setReactionModifier($reactionModifier)
    {
        $this->reactionModifier = $reactionModifier;

        return $this;
    }

    /**
     * Get reactionModifier
     *
     * @return integer
     */
    public function getReactionModifier()
    {
        return $this->reactionModifier;
    }

    /**
     * Set height
     *
     * @param float $height
     *
     * @return Character
     */
    public function setHeight($height)
    {
        $this->height = $height;

        return $this;
    }

    /**
     * Get height
     *
     * @return float
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * Set sizeModifier
     *
     * @param integer $sizeModifier
     *
     * @return Character
     */
    public function setSizeModifier($sizeModifier)
    {
        $this->sizeModifier = $sizeModifier;

        return $this;
    }

    /**
     * Get sizeModifier
     *
     * @return integer
     */
    public function getSizeModifier()
    {
        return $this->sizeModifier;
    }

    /**
     * Set strength
     *
     * @param integer $strength
     *
     * @return Character
     */
    public function setStrength($strength)
    {
        $this->strength = $strength;

        return $this;
    }

    /**
     * Get strength
     *
     * @return integer
     */
    public function getStrength()
    {
        return $this->strength;
    }

    /**
     * Set strengthModifier
     *
     * @param integer $strengthModifier
     *
     * @return Character
     */
    public function setStrengthModifier($strengthModifier)
    {
        $this->strengthModifier = $strengthModifier;

        return $this;
    }

    /**
     * Get strengthModifier
     *
     * @return integer
     */
    public function getStrengthModifier()
    {
        return $this->strengthModifier;
    }

    /**
     * Set dexterity
     *
     * @param integer $dexterity
     *
     * @return Character
     */
    public function setDexterity($dexterity)
    {
        $this->dexterity = $dexterity;

        return $this;
    }

    /**
     * Get dexterity
     *
     * @return integer
     */
    public function getDexterity()
    {
        return $this->dexterity;
    }

    /**
     * Set dexterityModifier
     *
     * @param integer $dexterityModifier
     *
     * @return Character
     */
    public function setDexterityModifier($dexterityModifier)
    {
        $this->dexterityModifier = $dexterityModifier;

        return $this;
    }

    /**
     * Get dexterityModifier
     *
     * @return integer
     */
    public function getDexterityModifier()
    {
        return $this->dexterityModifier;
    }

    /**
     * Set intelligence
     *
     * @param integer $intelligence
     *
     * @return Character
     */
    public function setIntelligence($intelligence)
    {
        $this->intelligence = $intelligence;

        return $this;
    }

    /**
     * Get intelligence
     *
     * @return integer
     */
    public function getIntelligence()
    {
        return $this->intelligence;
    }

    /**
     * Set intelligenceModifier
     *
     * @param integer $intelligenceModifier
     *
     * @return Character
     */
    public function setIntelligenceModifier($intelligenceModifier)
    {
        $this->intelligenceModifier = $intelligenceModifier;

        return $this;
    }

    /**
     * Get intelligenceModifier
     *
     * @return integer
     */
    public function getIntelligenceModifier()
    {
        return $this->intelligenceModifier;
    }

    /**
     * Set health
     *
     * @param integer $health
     *
     * @return Character
     */
    public function setHealth($health)
    {
        $this->health = $health;

        return $this;
    }

    /**
     * Get health
     *
     * @return integer
     */
    public function getHealth()
    {
        return $this->health;
    }

    /**
     * Set healthModifier
     *
     * @param integer $healthModifier
     *
     * @return Character
     */
    public function setHealthModifier($healthModifier)
    {
        $this->healthModifier = $healthModifier;

        return $this;
    }

    /**
     * Get healthModifier
     *
     * @return integer
     */
    public function getHealthModifier()
    {
        return $this->healthModifier;
    }

    /**
     * Set hitPoints
     *
     * @param integer $hitPoints
     *
     * @return Character
     */
    public function setHitPoints($hitPoints)
    {
        $this->hitPoints = $hitPoints;

        return $this;
    }

    /**
     * Get hitPoints
     *
     * @return integer
     */
    public function getHitPoints()
    {
        return $this->hitPoints;
    }

    /**
     * Set hitPointsModifier
     *
     * @param integer $hitPointsModifier
     *
     * @return Character
     */
    public function setHitPointsModifier($hitPointsModifier)
    {
        $this->hitPointsModifier = $hitPointsModifier;

        return $this;
    }

    /**
     * Get hitPointsModifier
     *
     * @return integer
     */
    public function getHitPointsModifier()
    {
        return $this->hitPointsModifier;
    }

    /**
     * Set hitPointsCurrent
     *
     * @param integer $hitPointsCurrent
     *
     * @return Character
     */
    public function setHitPointsCurrent($hitPointsCurrent)
    {
        $this->hitPointsCurrent = $hitPointsCurrent;

        return $this;
    }

    /**
     * Get hitPointsCurrent
     *
     * @return integer
     */
    public function getHitPointsCurrent()
    {
        return $this->hitPointsCurrent;
    }

    /**
     * Set will
     *
     * @param integer $will
     *
     * @return Character
     */
    public function setWill($will)
    {
        $this->will = $will;

        return $this;
    }

    /**
     * Get will
     *
     * @return integer
     */
    public function getWill()
    {
        return $this->will;
    }

    /**
     * Set willModifier
     *
     * @param integer $willModifier
     *
     * @return Character
     */
    public function setWillModifier($willModifier)
    {
        $this->willModifier = $willModifier;

        return $this;
    }

    /**
     * Get willModifier
     *
     * @return integer
     */
    public function getWillModifier()
    {
        return $this->willModifier;
    }

    /**
     * Set perception
     *
     * @param integer $perception
     *
     * @return Character
     */
    public function setPerception($perception)
    {
        $this->perception = $perception;

        return $this;
    }

    /**
     * Get perception
     *
     * @return integer
     */
    public function getPerception()
    {
        return $this->perception;
    }

    /**
     * Set perceptionModifier
     *
     * @param integer $perceptionModifier
     *
     * @return Character
     */
    public function setPerceptionModifier($perceptionModifier)
    {
        $this->perceptionModifier = $perceptionModifier;

        return $this;
    }

    /**
     * Get perceptionModifier
     *
     * @return integer
     */
    public function getPerceptionModifier()
    {
        return $this->perceptionModifier;
    }

    /**
     * Set fatiguePoints
     *
     * @param integer $fatiguePoints
     *
     * @return Character
     */
    public function setFatiguePoints($fatiguePoints)
    {
        $this->fatiguePoints = $fatiguePoints;

        return $this;
    }

    /**
     * Get fatiguePoints
     *
     * @return integer
     */
    public function getFatiguePoints()
    {
        return $this->fatiguePoints;
    }

    /**
     * Set fatiguePointsModifier
     *
     * @param integer $fatiguePointsModifier
     *
     * @return Character
     */
    public function setFatiguePointsModifier($fatiguePointsModifier)
    {
        $this->fatiguePointsModifier = $fatiguePointsModifier;

        return $this;
    }

    /**
     * Get fatiguePointsModifier
     *
     * @return integer
     */
    public function getFatiguePointsModifier()
    {
        return $this->fatiguePointsModifier;
    }

    /**
     * Set fatiguePointsCurrent
     *
     * @param integer $fatiguePointsCurrent
     *
     * @return Character
     */
    public function setFatiguePointsCurrent($fatiguePointsCurrent)
    {
        $this->fatiguePointsCurrent = $fatiguePointsCurrent;

        return $this;
    }

    /**
     * Get fatiguePointsCurrent
     *
     * @return integer
     */
    public function getFatiguePointsCurrent()
    {
        return $this->fatiguePointsCurrent;
    }

    /**
     * Set basicLift
     *
     * @param integer $basicLift
     *
     * @return Character
     */
    public function setBasicLift($basicLift)
    {
        $this->basicLift = $basicLift;

        return $this;
    }

    /**
     * Get basicLift
     *
     * @return integer
     */
    public function getBasicLift()
    {
        return $this->basicLift;
    }

    /**
     * Set basicSpeed
     *
     * @param float $basicSpeed
     *
     * @return Character
     */
    public function setBasicSpeed($basicSpeed)
    {
        $this->basicSpeed = $basicSpeed;

        return $this;
    }

    /**
     * Get basicSpeed
     *
     * @return float
     */
    public function getBasicSpeed()
    {
        return $this->basicSpeed;
    }

    /**
     * Set basicMove
     *
     * @param integer $basicMove
     *
     * @return Character
     */
    public function setBasicMove($basicMove)
    {
        $this->basicMove = $basicMove;

        return $this;
    }

    /**
     * Get basicMove
     *
     * @return integer
     */
    public function getBasicMove()
    {
        return $this->basicMove;
    }

    /**
     * Set hiking
     *
     * @param float $hiking
     *
     * @return Character
     */
    public function setHiking($hiking)
    {
        $this->hiking = $hiking;

        return $this;
    }

    /**
     * Get hiking
     *
     * @return float
     */
    public function getHiking()
    {
        return $this->hiking;
    }

    /**
     * Set gainedCP
     *
     * @param integer $gainedCP
     *
     * @return Character
     */
    public function setGainedCP($gainedCP)
    {
        $this->gainedCP = $gainedCP;

        return $this;
    }

    /**
     * Get gainedCP
     *
     * @return integer
     */
    public function getGainedCP()
    {
        return $this->gainedCP;
    }

    /**
     * Set startingCP
     *
     * @param integer $startingCP
     *
     * @return Character
     */
    public function setStartingCP($startingCP)
    {
        $this->startingCP = $startingCP;

        return $this;
    }

    /**
     * Get startingCP
     *
     * @return integer
     */
    public function getStartingCP()
    {
        return $this->startingCP;
    }

    /**
     * Set unusedCP
     *
     * @param integer $unusedCP
     *
     * @return Character
     */
    public function setUnusedCP($unusedCP)
    {
        $this->unusedCP = $unusedCP;

        return $this;
    }

    /**
     * Get unusedCP
     *
     * @return integer
     */
    public function getUnusedCP()
    {
        return $this->unusedCP;
    }

    /**
     * Set money
     *
     * @param integer $money
     *
     * @return Character
     */
    public function setMoney($money)
    {
        $this->money = $money;

        return $this;
    }

    /**
     * Get money
     *
     * @return integer
     */
    public function getMoney()
    {
        return $this->money;
    }

    /**
     * Set damageThrust
     *
     * @param string $damageThrust
     *
     * @return Character
     */
    public function setDamageThrust($damageThrust)
    {
        $this->damageThrust = $damageThrust;

        return $this;
    }

    /**
     * Get damageThrust
     *
     * @return string
     */
    public function getDamageThrust()
    {
        return $this->damageThrust;
    }

    /**
     * Set damageSwing
     *
     * @param string $damageSwing
     *
     * @return Character
     */
    public function setDamageSwing($damageSwing)
    {
        $this->damageSwing = $damageSwing;

        return $this;
    }

    /**
     * Get damageSwing
     *
     * @return string
     */
    public function getDamageSwing()
    {
        return $this->damageSwing;
    }

    /**
     * Set damageResistanceHead
     *
     * @param integer $damageResistanceHead
     *
     * @return Character
     */
    public function setDamageResistanceHead($damageResistanceHead)
    {
        $this->damageResistanceHead = $damageResistanceHead;

        return $this;
    }

    /**
     * Get damageResistanceHead
     *
     * @return integer
     */
    public function getDamageResistanceHead()
    {
        return $this->damageResistanceHead;
    }

    /**
     * Set damageResistanceBody
     *
     * @param integer $damageResistanceBody
     *
     * @return Character
     */
    public function setDamageResistanceBody($damageResistanceBody)
    {
        $this->damageResistanceBody = $damageResistanceBody;

        return $this;
    }

    /**
     * Get damageResistanceBody
     *
     * @return integer
     */
    public function getDamageResistanceBody()
    {
        return $this->damageResistanceBody;
    }

    /**
     * Set damageResistanceLeftArm
     *
     * @param integer $damageResistanceLeftArm
     *
     * @return Character
     */
    public function setDamageResistanceLeftArm($damageResistanceLeftArm)
    {
        $this->damageResistanceLeftArm = $damageResistanceLeftArm;

        return $this;
    }

    /**
     * Get damageResistanceLeftArm
     *
     * @return integer
     */
    public function getDamageResistanceLeftArm()
    {
        return $this->damageResistanceLeftArm;
    }

    /**
     * Set damageResistanceRightArm
     *
     * @param integer $damageResistanceRightArm
     *
     * @return Character
     */
    public function setDamageResistanceRightArm($damageResistanceRightArm)
    {
        $this->damageResistanceRightArm = $damageResistanceRightArm;

        return $this;
    }

    /**
     * Get damageResistanceRightArm
     *
     * @return integer
     */
    public function getDamageResistanceRightArm()
    {
        return $this->damageResistanceRightArm;
    }

    /**
     * Set damageResistanceLeftLeg
     *
     * @param integer $damageResistanceLeftLeg
     *
     * @return Character
     */
    public function setDamageResistanceLeftLeg($damageResistanceLeftLeg)
    {
        $this->damageResistanceLeftLeg = $damageResistanceLeftLeg;

        return $this;
    }

    /**
     * Get damageResistanceLeftLeg
     *
     * @return integer
     */
    public function getDamageResistanceLeftLeg()
    {
        return $this->damageResistanceLeftLeg;
    }

    /**
     * Set damageResistanceRightLeg
     *
     * @param integer $damageResistanceRightLeg
     *
     * @return Character
     */
    public function setDamageResistanceRightLeg($damageResistanceRightLeg)
    {
        $this->damageResistanceRightLeg = $damageResistanceRightLeg;

        return $this;
    }

    /**
     * Get damageResistanceRightLeg
     *
     * @return integer
     */
    public function getDamageResistanceRightLeg()
    {
        return $this->damageResistanceRightLeg;
    }

    /**
     * Set damageResistanceEyes
     *
     * @param integer $damageResistanceEyes
     *
     * @return Character
     */
    public function setDamageResistanceEyes($damageResistanceEyes)
    {
        $this->damageResistanceEyes = $damageResistanceEyes;

        return $this;
    }

    /**
     * Get damageResistanceEyes
     *
     * @return integer
     */
    public function getDamageResistanceEyes()
    {
        return $this->damageResistanceEyes;
    }

    /**
     * Set damageResistanceFace
     *
     * @param integer $damageResistanceFace
     *
     * @return Character
     */
    public function setDamageResistanceFace($damageResistanceFace)
    {
        $this->damageResistanceFace = $damageResistanceFace;

        return $this;
    }

    /**
     * Get damageResistanceFace
     *
     * @return integer
     */
    public function getDamageResistanceFace()
    {
        return $this->damageResistanceFace;
    }

    /**
     * Set energyResistanceHead
     *
     * @param integer $energyResistanceHead
     *
     * @return Character
     */
    public function setEnergyResistanceHead($energyResistanceHead)
    {
        $this->energyResistanceHead = $energyResistanceHead;

        return $this;
    }

    /**
     * Get energyResistanceHead
     *
     * @return integer
     */
    public function getEnergyResistanceHead()
    {
        return $this->energyResistanceHead;
    }

    /**
     * Set energyResistanceBody
     *
     * @param integer $energyResistanceBody
     *
     * @return Character
     */
    public function setEnergyResistanceBody($energyResistanceBody)
    {
        $this->energyResistanceBody = $energyResistanceBody;

        return $this;
    }

    /**
     * Get energyResistanceBody
     *
     * @return integer
     */
    public function getEnergyResistanceBody()
    {
        return $this->energyResistanceBody;
    }

    /**
     * Set energyResistanceLeftArm
     *
     * @param integer $energyResistanceLeftArm
     *
     * @return Character
     */
    public function setEnergyResistanceLeftArm($energyResistanceLeftArm)
    {
        $this->energyResistanceLeftArm = $energyResistanceLeftArm;

        return $this;
    }

    /**
     * Get energyResistanceLeftArm
     *
     * @return integer
     */
    public function getEnergyResistanceLeftArm()
    {
        return $this->energyResistanceLeftArm;
    }

    /**
     * Set energyResistanceRightArm
     *
     * @param integer $energyResistanceRightArm
     *
     * @return Character
     */
    public function setEnergyResistanceRightArm($energyResistanceRightArm)
    {
        $this->energyResistanceRightArm = $energyResistanceRightArm;

        return $this;
    }

    /**
     * Get energyResistanceRightArm
     *
     * @return integer
     */
    public function getEnergyResistanceRightArm()
    {
        return $this->energyResistanceRightArm;
    }

    /**
     * Set energyResistanceLeftLeg
     *
     * @param integer $energyResistanceLeftLeg
     *
     * @return Character
     */
    public function setEnergyResistanceLeftLeg($energyResistanceLeftLeg)
    {
        $this->energyResistanceLeftLeg = $energyResistanceLeftLeg;

        return $this;
    }

    /**
     * Get energyResistanceLeftLeg
     *
     * @return integer
     */
    public function getEnergyResistanceLeftLeg()
    {
        return $this->energyResistanceLeftLeg;
    }

    /**
     * Set energyResistanceRightLeg
     *
     * @param integer $energyResistanceRightLeg
     *
     * @return Character
     */
    public function setEnergyResistanceRightLeg($energyResistanceRightLeg)
    {
        $this->energyResistanceRightLeg = $energyResistanceRightLeg;

        return $this;
    }

    /**
     * Get energyResistanceRightLeg
     *
     * @return integer
     */
    public function getEnergyResistanceRightLeg()
    {
        return $this->energyResistanceRightLeg;
    }

    /**
     * Set energyResistanceEyes
     *
     * @param integer $energyResistanceEyes
     *
     * @return Character
     */
    public function setEnergyResistanceEyes($energyResistanceEyes)
    {
        $this->energyResistanceEyes = $energyResistanceEyes;

        return $this;
    }

    /**
     * Get energyResistanceEyes
     *
     * @return integer
     */
    public function getEnergyResistanceEyes()
    {
        return $this->energyResistanceEyes;
    }

    /**
     * Set energyResistanceFace
     *
     * @param integer $energyResistanceFace
     *
     * @return Character
     */
    public function setEnergyResistanceFace($energyResistanceFace)
    {
        $this->energyResistanceFace = $energyResistanceFace;

        return $this;
    }

    /**
     * Get energyResistanceFace
     *
     * @return integer
     */
    public function getEnergyResistanceFace()
    {
        return $this->energyResistanceFace;
    }

    /**
     * Set radiationResistanceHead
     *
     * @param integer $radiationResistanceHead
     *
     * @return Character
     */
    public function setRadiationResistanceHead($radiationResistanceHead)
    {
        $this->radiationResistanceHead = $radiationResistanceHead;

        return $this;
    }

    /**
     * Get radiationResistanceHead
     *
     * @return integer
     */
    public function getRadiationResistanceHead()
    {
        return $this->radiationResistanceHead;
    }

    /**
     * Set radiationResistanceBody
     *
     * @param integer $radiationResistanceBody
     *
     * @return Character
     */
    public function setRadiationResistanceBody($radiationResistanceBody)
    {
        $this->radiationResistanceBody = $radiationResistanceBody;

        return $this;
    }

    /**
     * Get radiationResistanceBody
     *
     * @return integer
     */
    public function getRadiationResistanceBody()
    {
        return $this->radiationResistanceBody;
    }

    /**
     * Set radiationResistanceLeftArm
     *
     * @param integer $radiationResistanceLeftArm
     *
     * @return Character
     */
    public function setRadiationResistanceLeftArm($radiationResistanceLeftArm)
    {
        $this->radiationResistanceLeftArm = $radiationResistanceLeftArm;

        return $this;
    }

    /**
     * Get radiationResistanceLeftArm
     *
     * @return integer
     */
    public function getRadiationResistanceLeftArm()
    {
        return $this->radiationResistanceLeftArm;
    }

    /**
     * Set radiationResistanceRightArm
     *
     * @param integer $radiationResistanceRightArm
     *
     * @return Character
     */
    public function setRadiationResistanceRightArm($radiationResistanceRightArm)
    {
        $this->radiationResistanceRightArm = $radiationResistanceRightArm;

        return $this;
    }

    /**
     * Get radiationResistanceRightArm
     *
     * @return integer
     */
    public function getRadiationResistanceRightArm()
    {
        return $this->radiationResistanceRightArm;
    }

    /**
     * Set radiationResistanceLeftLeg
     *
     * @param integer $radiationResistanceLeftLeg
     *
     * @return Character
     */
    public function setRadiationResistanceLeftLeg($radiationResistanceLeftLeg)
    {
        $this->radiationResistanceLeftLeg = $radiationResistanceLeftLeg;

        return $this;
    }

    /**
     * Get radiationResistanceLeftLeg
     *
     * @return integer
     */
    public function getRadiationResistanceLeftLeg()
    {
        return $this->radiationResistanceLeftLeg;
    }

    /**
     * Set radiationResistanceRightLeg
     *
     * @param integer $radiationResistanceRightLeg
     *
     * @return Character
     */
    public function setRadiationResistanceRightLeg($radiationResistanceRightLeg)
    {
        $this->radiationResistanceRightLeg = $radiationResistanceRightLeg;

        return $this;
    }

    /**
     * Get radiationResistanceRightLeg
     *
     * @return integer
     */
    public function getRadiationResistanceRightLeg()
    {
        return $this->radiationResistanceRightLeg;
    }

    /**
     * Set radiationResistanceEyes
     *
     * @param integer $radiationResistanceEyes
     *
     * @return Character
     */
    public function setRadiationResistanceEyes($radiationResistanceEyes)
    {
        $this->radiationResistanceEyes = $radiationResistanceEyes;

        return $this;
    }

    /**
     * Get radiationResistanceEyes
     *
     * @return integer
     */
    public function getRadiationResistanceEyes()
    {
        return $this->radiationResistanceEyes;
    }

    /**
     * Set radiationResistanceFace
     *
     * @param integer $radiationResistanceFace
     *
     * @return Character
     */
    public function setRadiationResistanceFace($radiationResistanceFace)
    {
        $this->radiationResistanceFace = $radiationResistanceFace;

        return $this;
    }

    /**
     * Get radiationResistanceFace
     *
     * @return integer
     */
    public function getRadiationResistanceFace()
    {
        return $this->radiationResistanceFace;
    }

    /**
     * Set parry
     *
     * @param integer $parry
     *
     * @return Character
     */
    public function setParry($parry)
    {
        $this->parry = $parry;

        return $this;
    }

    /**
     * Get parry
     *
     * @return integer
     */
    public function getParry()
    {
        return $this->parry;
    }

    /**
     * Set block
     *
     * @param integer $block
     *
     * @return Character
     */
    public function setBlock($block)
    {
        $this->block = $block;

        return $this;
    }

    /**
     * Get block
     *
     * @return integer
     */
    public function getBlock()
    {
        return $this->block;
    }

    /**
     * Set dodge
     *
     * @param integer $dodge
     *
     * @return Character
     */
    public function setDodge($dodge)
    {
        $this->dodge = $dodge;

        return $this;
    }

    /**
     * Get dodge
     *
     * @return integer
     */
    public function getDodge()
    {
        return $this->dodge;
    }

    /**
     * Set skillsCosts
     *
     * @param integer $skillsCosts
     *
     * @return Character
     */
    public function setSkillsCosts($skillsCosts)
    {
        $this->skillsCosts = $skillsCosts;

        return $this;
    }

    /**
     * Get skillsCosts
     *
     * @return integer
     */
    public function getSkillsCosts()
    {
        return $this->skillsCosts;
    }

    /**
     * Set skillsLevels
     *
     * @param array $skillsLevels
     *
     * @return Character
     */
    public function setSkillsLevels($skillsLevels)
    {
        $this->skillsLevels = $skillsLevels;

        return $this;
    }

    /**
     * Get skillsLevels
     *
     * @return array
     */
    public function getSkillsLevels()
    {
        return $this->skillsLevels;
    }

    /**
     * Set itemInventory
     *
     * @param array $itemInventory
     *
     * @return Character
     */
    public function setItemInventory($itemInventory)
    {
        $this->itemInventory = $itemInventory;

        return $this;
    }

    /**
     * Get itemInventory
     *
     * @return array
     */
    public function getItemInventory()
    {
        return $this->itemInventory;
    }

    /**
     * Set firearmInventory
     *
     * @param array $firearmInventory
     *
     * @return Character
     */
    public function setFirearmInventory($firearmInventory)
    {
        $this->firearmInventory = $firearmInventory;

        return $this;
    }

    /**
     * Get firearmInventory
     *
     * @return array
     */
    public function getFirearmInventory()
    {
        return $this->firearmInventory;
    }

    /**
     * Set wearableInventory
     *
     * @param array $wearableInventory
     *
     * @return Character
     */
    public function setWearableInventory($wearableInventory)
    {
        $this->wearableInventory = $wearableInventory;

        return $this;
    }

    /**
     * Get wearableInventory
     *
     * @return array
     */
    public function getWearableInventory()
    {
        return $this->wearableInventory;
    }

    /**
     * Set meeleeInventory
     *
     * @param array $meeleeInventory
     *
     * @return Character
     */
    public function setMeeleeInventory($meeleeInventory)
    {
        $this->meeleeInventory = $meeleeInventory;

        return $this;
    }

    /**
     * Get meeleeInventory
     *
     * @return array
     */
    public function getMeeleeInventory()
    {
        return $this->meeleeInventory;
    }

    /**
     * Set equip
     *
     * @param array $equip
     *
     * @return Character
     */
    public function setEquip($equip)
    {
        $this->equip = $equip;

        return $this;
    }

    /**
     * Get equip
     *
     * @return array
     */
    public function getEquip()
    {
        return $this->equip;
    }

    /**
     * Set naturalAttacks
     *
     * @param array $naturalAttacks
     *
     * @return Character
     */
    public function setNaturalAttacks($naturalAttacks)
    {
        $this->naturalAttacks = $naturalAttacks;

        return $this;
    }

    /**
     * Get naturalAttacks
     *
     * @return array
     */
    public function getNaturalAttacks()
    {
        return $this->naturalAttacks;
    }

    /**
     * Set effects
     *
     * @param array $effects
     *
     * @return Character
     */
    public function setEffects($effects)
    {
        $this->effects = $effects;

        return $this;
    }

    /**
     * Get effects
     *
     * @return array
     */
    public function getEffects()
    {
        return $this->effects;
    }

    /**
     * Set encumbranceLevel
     *
     * @param integer $encumbranceLevel
     *
     * @return Character
     */
    public function setEncumbranceLevel($encumbranceLevel)
    {
        $this->encumbranceLevel = $encumbranceLevel;

        return $this;
    }

    /**
     * Get encumbranceLevel
     *
     * @return integer
     */
    public function getEncumbranceLevel()
    {
        return $this->encumbranceLevel;
    }

    /**
     * Get imageSize
     *
     * @return integer
     */
    public function getImageSize()
    {
        return $this->imageSize;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Character
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Add world
     *
     * @param \AppBundle\Entity\World $world
     *
     * @return Character
     */
    public function addWorld(\AppBundle\Entity\World $world)
    {
        $this->world[] = $world;

        return $this;
    }

    /**
     * Remove world
     *
     * @param \AppBundle\Entity\World $world
     */
    public function removeWorld(\AppBundle\Entity\World $world)
    {
        $this->world->removeElement($world);
    }

    /**
     * Get world
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getWorld()
    {
        return $this->world;
    }
}

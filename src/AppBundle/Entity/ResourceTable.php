<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="resourceTable")
 */
class ResourceTable
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     */
    private $name;
    /**
     * @ORM\Column(type="json_array")
     */
    private $content;

    public function getId()
    {
      return $this->id;
    }
    public function getName()
    {
      return $this->name;
    }
    public function setName($name)
    {
      $this->name = $name;
      return $this;
    }

    public function getContent()
    {
      return $this->content;
    }
    public function setContent($content)
    {
      $this->content = $content;
      return $this;
    }


}

<?php
namespace AppBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

class WearableRepository extends EntityRepository
{
    public function getByWorld($id)
    {
              return $this->createQueryBuilder('a')
            ->innerJoin('a.world', 'b')
            ->andWhere('b.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getResult();
    }
}

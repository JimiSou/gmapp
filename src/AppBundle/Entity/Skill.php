<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\SkillRepository")
 * @ORM\Table(name="skill")
 * @Vich\Uploadable
 */
class Skill
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     */
    private $name;
    /**
     * @ORM\Column(type="text")
     */
    private $difficulty;
    /**
     * @ORM\Column(type="text")
     */
    private $attribute;
    /**
     * @ORM\Column(type="integer")
     */
    private $defaultModifier;
    /**
     * @ORM\Column(type="integer")
     */
    private $cost;
    /**
     * @ORM\Column(type="text")
     */
    private $description;
    /**
     * @ORM\Column(type="json_array")
     */
    private $effect=[];
    /**
     * @ORM\ManyToMany(targetEntity="World")
     * @ORM\JoinTable(name="skill_worlds_join",
     *      joinColumns={@ORM\JoinColumn(name="skill_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="world_id", referencedColumnName="id")}
     *      )
     */
    private $world;
    /**
     *
     *
     * @Vich\UploadableField(mapping="skill_image", fileNameProperty="imageName", size="imageSize")
     *
     * @var File
     */
    private $imageFile;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @var string
     */
    private $imageName;
    /**
     * @ORM\Column(type="integer")
     *
     * @var integer
     */
    private $imageSize=0;

    /**
     * @ORM\Column(type="datetime")
     *
     * @var \DateTime
     */
    private $updatedAt;

    /**
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     *
     * @return Product
     */
    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;

        if ($image) {
          $this->updatedAt = new \DateTimeImmutable();
        }

        return $this;
    }

    /**
     * @return File|null
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    /**
     * @param string $imageName
     *
     * @return Product
     */
    public function setImageName($imageName)
    {
        $this->imageName = $imageName;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getImageName()
    {
        return $this->imageName;
    }

    /**
     * @param integer $imageSize
     *
     * @return Product
     */
    public function setImageSize($imageSize)
    {
        $this->imageSize = $imageSize;

        return $this;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->world = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Skill
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set difficulty
     *
     * @param string $difficulty
     *
     * @return Skill
     */
    public function setDifficulty($difficulty)
    {
        $this->difficulty = $difficulty;

        return $this;
    }

    /**
     * Get difficulty
     *
     * @return string
     */
    public function getDifficulty()
    {
        return $this->difficulty;
    }

    /**
     * Set attribute
     *
     * @param string $attribute
     *
     * @return Skill
     */
    public function setAttribute($attribute)
    {
        $this->attribute = $attribute;

        return $this;
    }

    /**
     * Get attribute
     *
     * @return string
     */
    public function getAttribute()
    {
        return $this->attribute;
    }

    /**
     * Set defaultModifier
     *
     * @param integer $defaultModifier
     *
     * @return Skill
     */
    public function setDefaultModifier($defaultModifier)
    {
        $this->defaultModifier = $defaultModifier;

        return $this;
    }

    /**
     * Get defaultModifier
     *
     * @return integer
     */
    public function getDefaultModifier()
    {
        return $this->defaultModifier;
    }

    /**
     * Set cost
     *
     * @param integer $cost
     *
     * @return Skill
     */
    public function setCost($cost)
    {
        $this->cost = $cost;

        return $this;
    }

    /**
     * Get cost
     *
     * @return integer
     */
    public function getCost()
    {
        return $this->cost;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Skill
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set effect
     *
     * @param array $effect
     *
     * @return Skill
     */
    public function setEffect($effect)
    {
        $this->effect = $effect;

        return $this;
    }

    /**
     * Get effect
     *
     * @return array
     */
    public function getEffect()
    {
        return $this->effect;
    }

    /**
     * Get imageSize
     *
     * @return integer
     */
    public function getImageSize()
    {
        return $this->imageSize;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Skill
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Add world
     *
     * @param \AppBundle\Entity\World $world
     *
     * @return Skill
     */
    public function addWorld(\AppBundle\Entity\World $world)
    {
        $this->world[] = $world;

        return $this;
    }

    /**
     * Remove world
     *
     * @param \AppBundle\Entity\World $world
     */
    public function removeWorld(\AppBundle\Entity\World $world)
    {
        $this->world->removeElement($world);
    }

    /**
     * Get world
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getWorld()
    {
        return $this->world;
    }
}

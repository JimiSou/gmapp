<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\FactionRepository")
 * @ORM\Table(name="faction")
 * @Vich\Uploadable
 */
class Faction
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     */
    private $name;
    /**
     * @ORM\Column(type="text")
     */
    private $description;
    /**
     * @ORM\ManyToMany(targetEntity="Location")
     * @ORM\JoinTable(name="factions_locations",
     *      joinColumns={@ORM\JoinColumn(name="faction_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="location_id", referencedColumnName="id")}
     *      )
     */
    private $relatedLocations;
    /**
     * @ORM\ManyToMany(targetEntity="Faction", mappedBy="allies")
     */
    private $alliedWithMe;

    /**
     * @ORM\ManyToMany(targetEntity="Faction", inversedBy="alliedWithMe")
     * @ORM\JoinTable(name="factionAllies",
     *      joinColumns={@ORM\JoinColumn(name="faction_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="allied_faction_id", referencedColumnName="id")}
     *      )
     */
    private $allies;
    /**
     * @ORM\ManyToMany(targetEntity="World")
     * @ORM\JoinTable(name="faction_worlds_join",
     *      joinColumns={@ORM\JoinColumn(name="faction_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="world_id", referencedColumnName="id")}
     *      )
     */
    private $worlds;
    /**
     *
     *
     * @Vich\UploadableField(mapping="faction_image", fileNameProperty="imageName", size="imageSize")
     *
     * @var File
     */
    private $imageFile;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @var string
     */
    private $imageName;
    /**
     * @ORM\Column(type="integer")
     *
     * @var integer
     */
    private $imageSize=0;

    /**
     * @ORM\Column(type="datetime")
     *
     * @var \DateTime
     */
    private $updatedAt;

    /**
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     *
     * @return Product
     */
    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;

        if ($image) {
          $this->updatedAt = new \DateTimeImmutable();
        }

        return $this;
    }

    /**
     * @return File|null
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    /**
     * @param string $imageName
     *
     * @return Product
     */
    public function setImageName($imageName)
    {
        $this->imageName = $imageName;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getImageName()
    {
        return $this->imageName;
    }

    /**
     * @param integer $imageSize
     *
     * @return Product
     */
    public function setImageSize($imageSize)
    {
        $this->imageSize = $imageSize;

        return $this;
    }
      /**
     * Constructor
     */
    public function __construct()
    {
        $this->relatedLocations = new \Doctrine\Common\Collections\ArrayCollection();
        $this->alliedWithMe = new \Doctrine\Common\Collections\ArrayCollection();
        $this->allies = new \Doctrine\Common\Collections\ArrayCollection();
        $this->worlds = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Faction
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Faction
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Get imageSize
     *
     * @return integer
     */
    public function getImageSize()
    {
        return $this->imageSize;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Faction
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Add relatedLocation
     *
     * @param \AppBundle\Entity\Location $relatedLocation
     *
     * @return Faction
     */
    public function addRelatedLocation(\AppBundle\Entity\Location $relatedLocation)
    {
        $this->relatedLocations[] = $relatedLocation;

        return $this;
    }

    /**
     * Remove relatedLocation
     *
     * @param \AppBundle\Entity\Location $relatedLocation
     */
    public function removeRelatedLocation(\AppBundle\Entity\Location $relatedLocation)
    {
        $this->relatedLocations->removeElement($relatedLocation);
    }

    /**
     * Get relatedLocations
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRelatedLocations()
    {
        return $this->relatedLocations;
    }

    /**
     * Add alliedWithMe
     *
     * @param \AppBundle\Entity\Faction $alliedWithMe
     *
     * @return Faction
     */
    public function addAlliedWithMe(\AppBundle\Entity\Faction $alliedWithMe)
    {
        $this->alliedWithMe[] = $alliedWithMe;

        return $this;
    }

    /**
     * Remove alliedWithMe
     *
     * @param \AppBundle\Entity\Faction $alliedWithMe
     */
    public function removeAlliedWithMe(\AppBundle\Entity\Faction $alliedWithMe)
    {
        $this->alliedWithMe->removeElement($alliedWithMe);
    }

    /**
     * Get alliedWithMe
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAlliedWithMe()
    {
        return $this->alliedWithMe;
    }

    /**
     * Add ally
     *
     * @param \AppBundle\Entity\Faction $ally
     *
     * @return Faction
     */
    public function addAlly(\AppBundle\Entity\Faction $ally)
    {
        $this->allies[] = $ally;

        return $this;
    }

    /**
     * Remove ally
     *
     * @param \AppBundle\Entity\Faction $ally
     */
    public function removeAlly(\AppBundle\Entity\Faction $ally)
    {
        $this->allies->removeElement($ally);
    }

    /**
     * Get allies
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAllies()
    {
        return $this->allies;
    }

    /**
     * Add world
     *
     * @param \AppBundle\Entity\World $world
     *
     * @return Faction
     */
    public function addWorld(\AppBundle\Entity\World $world)
    {
        $this->worlds[] = $world;

        return $this;
    }

    /**
     * Remove world
     *
     * @param \AppBundle\Entity\World $world
     */
    public function removeWorld(\AppBundle\Entity\World $world)
    {
        $this->worlds->removeElement($world);
    }

    /**
     * Get worlds
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getWorlds()
    {
        return $this->worlds;
    }
}

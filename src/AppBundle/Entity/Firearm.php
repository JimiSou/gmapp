<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\FirearmRepository")
 * @ORM\Table(name="firearm")
 * @Vich\Uploadable
 */
class Firearm
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     */
    private $name;
    /**
     * @ORM\Column(type="text")
     */
    private $damage;
    /**
     * @ORM\Column(type="text")
     */
    private $damageType;
    /**
     * @ORM\Column(type="text")
     */
    private $accuracy;
    /**
     * @ORM\Column(type="integer")
     */
    private $rangeMin;
    /**
     * @ORM\Column(type="integer")
     */
    private $rangeMax;
    /**
     * @ORM\Column(type="float")
     */
    private $weight;
    /**
     * @ORM\Column(type="text")
     */
    private $rateOfFire;
    /**
     * @ORM\Column(type="text")
     */
    private $shots;
    /**
     * @ORM\Column(type="text")
     */
    private $requiredStrength;
    /**
     * @ORM\Column(type="integer")
     */
    private $bulk;
    /**
     * @ORM\Column(type="integer")
     */
    private $recoil;
    /**
     * @ORM\Column(type="integer")
     */
    private $cost;
    /**
     * @ORM\Column(type="text")
     */
    private $ammo;
    /**
     * @ORM\Column(type="text")
     */
    private $category;
    /**
     * @ORM\Column(type="integer")
     */
    private $technologyLevel;
    /**
     * @ORM\ManyToOne(targetEntity="Skill")
     */
    private $skill;
    /**
     * @ORM\ManyToMany(targetEntity="World")
     * @ORM\JoinTable(name="firearm_worlds_join",
     *      joinColumns={@ORM\JoinColumn(name="firearm_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="world_id", referencedColumnName="id")}
     *      )
     */
    private $world;
    /**
     *
     *
     * @Vich\UploadableField(mapping="firearm_image", fileNameProperty="imageName", size="imageSize")
     *
     * @var File
     */
    private $imageFile;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @var string
     */
    private $imageName;
    /**
     * @ORM\Column(type="integer")
     *
     * @var integer
     */
    private $imageSize=0;

    /**
     * @ORM\Column(type="datetime")
     *
     * @var \DateTime
     */
    private $updatedAt;

    /**
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     *
     * @return Product
     */
    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;

        if ($image) {
          $this->updatedAt = new \DateTimeImmutable();
        }

        return $this;
    }

    /**
     * @return File|null
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    /**
     * @param string $imageName
     *
     * @return Product
     */
    public function setImageName($imageName)
    {
        $this->imageName = $imageName;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getImageName()
    {
        return $this->imageName;
    }

    /**
     * @param integer $imageSize
     *
     * @return Product
     */
    public function setImageSize($imageSize)
    {
        $this->imageSize = $imageSize;

        return $this;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->world = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Firearm
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set damage
     *
     * @param string $damage
     *
     * @return Firearm
     */
    public function setDamage($damage)
    {
        $this->damage = $damage;

        return $this;
    }

    /**
     * Get damage
     *
     * @return string
     */
    public function getDamage()
    {
        return $this->damage;
    }

    /**
     * Set damageType
     *
     * @param string $damageType
     *
     * @return Firearm
     */
    public function setDamageType($damageType)
    {
        $this->damageType = $damageType;

        return $this;
    }

    /**
     * Get damageType
     *
     * @return string
     */
    public function getDamageType()
    {
        return $this->damageType;
    }

    /**
     * Set accuracy
     *
     * @param string $accuracy
     *
     * @return Firearm
     */
    public function setAccuracy($accuracy)
    {
        $this->accuracy = $accuracy;

        return $this;
    }

    /**
     * Get accuracy
     *
     * @return string
     */
    public function getAccuracy()
    {
        return $this->accuracy;
    }

    /**
     * Set rangeMin
     *
     * @param integer $rangeMin
     *
     * @return Firearm
     */
    public function setRangeMin($rangeMin)
    {
        $this->rangeMin = $rangeMin;

        return $this;
    }

    /**
     * Get rangeMin
     *
     * @return integer
     */
    public function getRangeMin()
    {
        return $this->rangeMin;
    }

    /**
     * Set rangeMax
     *
     * @param integer $rangeMax
     *
     * @return Firearm
     */
    public function setRangeMax($rangeMax)
    {
        $this->rangeMax = $rangeMax;

        return $this;
    }

    /**
     * Get rangeMax
     *
     * @return integer
     */
    public function getRangeMax()
    {
        return $this->rangeMax;
    }

    /**
     * Set weight
     *
     * @param float $weight
     *
     * @return Firearm
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;

        return $this;
    }

    /**
     * Get weight
     *
     * @return float
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * Set rateOfFire
     *
     * @param string $rateOfFire
     *
     * @return Firearm
     */
    public function setRateOfFire($rateOfFire)
    {
        $this->rateOfFire = $rateOfFire;

        return $this;
    }

    /**
     * Get rateOfFire
     *
     * @return string
     */
    public function getRateOfFire()
    {
        return $this->rateOfFire;
    }

    /**
     * Set shots
     *
     * @param string $shots
     *
     * @return Firearm
     */
    public function setShots($shots)
    {
        $this->shots = $shots;

        return $this;
    }

    /**
     * Get shots
     *
     * @return string
     */
    public function getShots()
    {
        return $this->shots;
    }

    /**
     * Set requiredStrength
     *
     * @param string $requiredStrength
     *
     * @return Firearm
     */
    public function setRequiredStrength($requiredStrength)
    {
        $this->requiredStrength = $requiredStrength;

        return $this;
    }

    /**
     * Get requiredStrength
     *
     * @return string
     */
    public function getRequiredStrength()
    {
        return $this->requiredStrength;
    }

    /**
     * Set bulk
     *
     * @param integer $bulk
     *
     * @return Firearm
     */
    public function setBulk($bulk)
    {
        $this->bulk = $bulk;

        return $this;
    }

    /**
     * Get bulk
     *
     * @return integer
     */
    public function getBulk()
    {
        return $this->bulk;
    }

    /**
     * Set recoil
     *
     * @param integer $recoil
     *
     * @return Firearm
     */
    public function setRecoil($recoil)
    {
        $this->recoil = $recoil;

        return $this;
    }

    /**
     * Get recoil
     *
     * @return integer
     */
    public function getRecoil()
    {
        return $this->recoil;
    }

    /**
     * Set cost
     *
     * @param integer $cost
     *
     * @return Firearm
     */
    public function setCost($cost)
    {
        $this->cost = $cost;

        return $this;
    }

    /**
     * Get cost
     *
     * @return integer
     */
    public function getCost()
    {
        return $this->cost;
    }

    /**
     * Set ammo
     *
     * @param string $ammo
     *
     * @return Firearm
     */
    public function setAmmo($ammo)
    {
        $this->ammo = $ammo;

        return $this;
    }

    /**
     * Get ammo
     *
     * @return string
     */
    public function getAmmo()
    {
        return $this->ammo;
    }

    /**
     * Set category
     *
     * @param string $category
     *
     * @return Firearm
     */
    public function setCategory($category)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return string
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set technologyLevel
     *
     * @param integer $technologyLevel
     *
     * @return Firearm
     */
    public function setTechnologyLevel($technologyLevel)
    {
        $this->technologyLevel = $technologyLevel;

        return $this;
    }

    /**
     * Get technologyLevel
     *
     * @return integer
     */
    public function getTechnologyLevel()
    {
        return $this->technologyLevel;
    }

    /**
     * Get imageSize
     *
     * @return integer
     */
    public function getImageSize()
    {
        return $this->imageSize;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Firearm
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Add world
     *
     * @param \AppBundle\Entity\World $world
     *
     * @return Firearm
     */
    public function addWorld(\AppBundle\Entity\World $world)
    {
        $this->world[] = $world;

        return $this;
    }

    /**
     * Remove world
     *
     * @param \AppBundle\Entity\World $world
     */
    public function removeWorld(\AppBundle\Entity\World $world)
    {
        $this->world->removeElement($world);
    }

    /**
     * Get world
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getWorld()
    {
        return $this->world;
    }

    /**
     * Set the value of Id
     *
     * @param mixed id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of Skill
     *
     * @return mixed
     */
    public function getSkill()
    {
        return $this->skill;
    }

    /**
     * Set the value of Skill
     *
     * @param mixed skill
     *
     * @return self
     */
    public function setSkill($skill)
    {
        $this->skill = $skill;

        return $this;
    }

    /**
     * Set the value of World
     *
     * @param mixed world
     *
     * @return self
     */
    public function setWorld($world)
    {
        $this->world = $world;

        return $this;
    }

}

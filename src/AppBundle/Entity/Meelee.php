<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\MeeleeRepository")
 * @ORM\Table(name="meelee")
 * @Vich\Uploadable
 */
class Meelee
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     */
    private $name;
    /**
     * @ORM\Column(type="text")
     */
    private $thrustDamage;
    /**
     * @ORM\Column(type="text")
     */
    private	$thrustReach;
    /**
     * @ORM\Column(type="text")
     */
    private	$swingDamage;
    /**
     * @ORM\Column(type="text")
     */
    private	$swingReach;
    /**
     * @ORM\Column(type="text")
     */
    private	$parry;
    /**
     * @ORM\Column(type="integer")
     */
    private	$cost;
    /**
     * @ORM\Column(type="float")
     */
    private	$weight;
    /**
     * @ORM\Column(type="integer")
     */
    private $strength;
    /**
     * @ORM\Column(type="text")
     */
    private	$notes;
    /**
     * @ORM\Column(type="integer")
     */
    private	$technologyLevel;
    /**
     * @ORM\Column(type="text")
     */
    private	$category;
    /**
     * @ORM\ManyToOne(targetEntity="Skill")
     */
    private $skill;
    /**
     * @ORM\ManyToMany(targetEntity="World")
     * @ORM\JoinTable(name="meelee_worlds_join",
     *      joinColumns={@ORM\JoinColumn(name="meelee_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="world_id", referencedColumnName="id")}
     *      )
     */
    private $world;
    /**
     *
     *
     * @Vich\UploadableField(mapping="meelee_image", fileNameProperty="imageName", size="imageSize")
     *
     * @var File
     */
    private $imageFile;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @var string
     */
    private $imageName;
    /**
     * @ORM\Column(type="integer")
     *
     * @var integer
     */
    private $imageSize=0;

    /**
     * @ORM\Column(type="datetime")
     *
     * @var \DateTime
     */
    private $updatedAt;

    /**
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     *
     * @return Product
     */
    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;

        if ($image) {
          $this->updatedAt = new \DateTimeImmutable();
        }

        return $this;
    }

    /**
     * @return File|null
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    /**
     * @param string $imageName
     *
     * @return Product
     */
    public function setImageName($imageName)
    {
        $this->imageName = $imageName;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getImageName()
    {
        return $this->imageName;
    }

    /**
     * @param integer $imageSize
     *
     * @return Product
     */
    public function setImageSize($imageSize)
    {
        $this->imageSize = $imageSize;

        return $this;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->world = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Meelee
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set thrustDamage
     *
     * @param string $thrustDamage
     *
     * @return Meelee
     */
    public function setThrustDamage($thrustDamage)
    {
        $this->thrustDamage = $thrustDamage;

        return $this;
    }

    /**
     * Get thrustDamage
     *
     * @return string
     */
    public function getThrustDamage()
    {
        return $this->thrustDamage;
    }

    /**
     * Set thrustReach
     *
     * @param string $thrustReach
     *
     * @return Meelee
     */
    public function setThrustReach($thrustReach)
    {
        $this->thrustReach = $thrustReach;

        return $this;
    }

    /**
     * Get thrustReach
     *
     * @return string
     */
    public function getThrustReach()
    {
        return $this->thrustReach;
    }

    /**
     * Set swingDamage
     *
     * @param string $swingDamage
     *
     * @return Meelee
     */
    public function setSwingDamage($swingDamage)
    {
        $this->swingDamage = $swingDamage;

        return $this;
    }

    /**
     * Get swingDamage
     *
     * @return string
     */
    public function getSwingDamage()
    {
        return $this->swingDamage;
    }

    /**
     * Set swingReach
     *
     * @param string $swingReach
     *
     * @return Meelee
     */
    public function setSwingReach($swingReach)
    {
        $this->swingReach = $swingReach;

        return $this;
    }

    /**
     * Get swingReach
     *
     * @return string
     */
    public function getSwingReach()
    {
        return $this->swingReach;
    }

    /**
     * Set parry
     *
     * @param string $parry
     *
     * @return Meelee
     */
    public function setParry($parry)
    {
        $this->parry = $parry;

        return $this;
    }

    /**
     * Get parry
     *
     * @return string
     */
    public function getParry()
    {
        return $this->parry;
    }

    /**
     * Set cost
     *
     * @param integer $cost
     *
     * @return Meelee
     */
    public function setCost($cost)
    {
        $this->cost = $cost;

        return $this;
    }

    /**
     * Get cost
     *
     * @return integer
     */
    public function getCost()
    {
        return $this->cost;
    }

    /**
     * Set weight
     *
     * @param float $weight
     *
     * @return Meelee
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;

        return $this;
    }

    /**
     * Get weight
     *
     * @return float
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * Set strength
     *
     * @param integer $strength
     *
     * @return Meelee
     */
    public function setStrength($strength)
    {
        $this->strength = $strength;

        return $this;
    }

    /**
     * Get strength
     *
     * @return integer
     */
    public function getStrength()
    {
        return $this->strength;
    }

    /**
     * Set notes
     *
     * @param string $notes
     *
     * @return Meelee
     */
    public function setNotes($notes)
    {
        $this->notes = $notes;

        return $this;
    }

    /**
     * Get notes
     *
     * @return string
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * Set technologyLevel
     *
     * @param integer $technologyLevel
     *
     * @return Meelee
     */
    public function setTechnologyLevel($technologyLevel)
    {
        $this->technologyLevel = $technologyLevel;

        return $this;
    }

    /**
     * Get technologyLevel
     *
     * @return integer
     */
    public function getTechnologyLevel()
    {
        return $this->technologyLevel;
    }

    /**
     * Set category
     *
     * @param string $category
     *
     * @return Meelee
     */
    public function setCategory($category)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return string
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Get imageSize
     *
     * @return integer
     */
    public function getImageSize()
    {
        return $this->imageSize;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Meelee
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Add world
     *
     * @param \AppBundle\Entity\World $world
     *
     * @return Meelee
     */
    public function addWorld(\AppBundle\Entity\World $world)
    {
        $this->world[] = $world;

        return $this;
    }

    /**
     * Remove world
     *
     * @param \AppBundle\Entity\World $world
     */
    public function removeWorld(\AppBundle\Entity\World $world)
    {
        $this->world->removeElement($world);
    }

    /**
     * Get world
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getWorld()
    {
        return $this->world;
    }

    /**
     * Set the value of Id
     *
     * @param mixed id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of Skill
     *
     * @return mixed
     */
    public function getSkill()
    {
        return $this->skill;
    }

    /**
     * Set the value of Skill
     *
     * @param mixed skill
     *
     * @return self
     */
    public function setSkill($skill)
    {
        $this->skill = $skill;

        return $this;
    }

    /**
     * Set the value of World
     *
     * @param mixed world
     *
     * @return self
     */
    public function setWorld($world)
    {
        $this->world = $world;

        return $this;
    }

}

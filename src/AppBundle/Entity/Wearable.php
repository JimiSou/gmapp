<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\WearableRepository")
 * @ORM\Table(name="wearable")
 * @Vich\Uploadable
 */
class Wearable
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     */
    private $name;
    /**
     * @ORM\Column(type="integer")
     */
    private $damageResist;
    /**
     * @ORM\Column(type="integer")
     */
    private $energyResist;
    /**
     * @ORM\Column(type="integer")
     */
    private $radiationResist;
    /**
     * @ORM\Column(type="text")
     */
    private $effect;
    /**
     * @ORM\Column(type="integer")
     */
    private $cost;
    /**
     * @ORM\Column(type="float")
     */
    private $weight;
    /**
     * @ORM\Column(type="json_array")
     */
    private $slots;
    /**
     * @ORM\Column(type="text")
     */
    private $type;
    /**
     * @ORM\ManyToMany(targetEntity="World")
     * @ORM\JoinTable(name="wearable_worlds_join",
     *      joinColumns={@ORM\JoinColumn(name="wearable_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="world_id", referencedColumnName="id")}
     *      )
     */
    private $world;
    /**
     *
     *
     * @Vich\UploadableField(mapping="wearable_image", fileNameProperty="imageName", size="imageSize")
     *
     * @var File
     */
    private $imageFile;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @var string
     */
    private $imageName;
    /**
     * @ORM\Column(type="integer")
     *
     * @var integer
     */
    private $imageSize=0;

    /**
     * @ORM\Column(type="datetime")
     *
     * @var \DateTime
     */
    private $updatedAt;

    /**
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     *
     * @return Product
     */
    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;

        if ($image) {
          $this->updatedAt = new \DateTimeImmutable();
        }

        return $this;
    }

    /**
     * @return File|null
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    /**
     * @param string $imageName
     *
     * @return Product
     */
    public function setImageName($imageName)
    {
        $this->imageName = $imageName;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getImageName()
    {
        return $this->imageName;
    }

    /**
     * @param integer $imageSize
     *
     * @return Product
     */
    public function setImageSize($imageSize)
    {
        $this->imageSize = $imageSize;

        return $this;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->world = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Wearable
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set damageResist
     *
     * @param integer $damageResist
     *
     * @return Wearable
     */
    public function setDamageResist($damageResist)
    {
        $this->damageResist = $damageResist;

        return $this;
    }

    /**
     * Get damageResist
     *
     * @return integer
     */
    public function getDamageResist()
    {
        return $this->damageResist;
    }

    /**
     * Set energyResist
     *
     * @param integer $energyResist
     *
     * @return Wearable
     */
    public function setEnergyResist($energyResist)
    {
        $this->energyResist = $energyResist;

        return $this;
    }

    /**
     * Get energyResist
     *
     * @return integer
     */
    public function getEnergyResist()
    {
        return $this->energyResist;
    }

    /**
     * Set radiationResist
     *
     * @param integer $radiationResist
     *
     * @return Wearable
     */
    public function setRadiationResist($radiationResist)
    {
        $this->radiationResist = $radiationResist;

        return $this;
    }

    /**
     * Get radiationResist
     *
     * @return integer
     */
    public function getRadiationResist()
    {
        return $this->radiationResist;
    }

    /**
     * Set effect
     *
     * @param string $effect
     *
     * @return Wearable
     */
    public function setEffect($effect)
    {
        $this->effect = $effect;

        return $this;
    }

    /**
     * Get effect
     *
     * @return string
     */
    public function getEffect()
    {
        return $this->effect;
    }

    /**
     * Set cost
     *
     * @param integer $cost
     *
     * @return Wearable
     */
    public function setCost($cost)
    {
        $this->cost = $cost;

        return $this;
    }

    /**
     * Get cost
     *
     * @return integer
     */
    public function getCost()
    {
        return $this->cost;
    }

    /**
     * Set weight
     *
     * @param float $weight
     *
     * @return Wearable
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;

        return $this;
    }

    /**
     * Get weight
     *
     * @return float
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * Set slots
     *
     * @param array $slots
     *
     * @return Wearable
     */
    public function setSlots($slots)
    {
        $this->slots = $slots;

        return $this;
    }

    /**
     * Get slots
     *
     * @return array
     */
    public function getSlots()
    {
        return $this->slots;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Wearable
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Get imageSize
     *
     * @return integer
     */
    public function getImageSize()
    {
        return $this->imageSize;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Wearable
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Add world
     *
     * @param \AppBundle\Entity\World $world
     *
     * @return Wearable
     */
    public function addWorld(\AppBundle\Entity\World $world)
    {
        $this->world[] = $world;

        return $this;
    }

    /**
     * Remove world
     *
     * @param \AppBundle\Entity\World $world
     */
    public function removeWorld(\AppBundle\Entity\World $world)
    {
        $this->world->removeElement($world);
    }

    /**
     * Get world
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getWorld()
    {
        return $this->world;
    }
}
